package io.gitlab.guoda.galenjava.mobile.core;

import com.galenframework.browser.Browser;
import com.galenframework.specs.page.Locator;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface GalenLayoutAsserter {
    void checkLayout(String specPath, List<String> includedTags, Map<String, Locator> galenLocators) throws IOException;
}
