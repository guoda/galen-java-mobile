package io.gitlab.guoda.galenjava.mobile.locator;

public class GalenLocatorException extends Exception {
    public GalenLocatorException() {
        super();
    }

    public GalenLocatorException(String message) {
        super(message);
    }
}
