package io.gitlab.guoda.galenjava.mobile.testng;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class AndroidTestDeviceConfigurationTest {
    private static TestDevice mockDevice0, mockDevice1, mockDevice2, mockDevice3, mockDevice4, mockDevice5;

    @BeforeAll
    static void mockSelenium() {
        mockDevice0 = mock(AndroidTestDevice.class);
        mockDevice1 = mock(AndroidTestDevice.class);
        mockDevice2 = mock(AndroidTestDevice.class);
        mockDevice3 = mock(AndroidTestDevice.class);
        mockDevice4 = mock(AndroidTestDevice.class);
        mockDevice5 = mock(AndroidTestDevice.class);


        // assume device screen height received from driver is 1000
        when(mockDevice0.getScreenHeight()).thenReturn(1000);
        when(mockDevice0.getName()).thenReturn("device0");
        when(mockDevice1.getScreenHeight()).thenReturn(1000);
        when(mockDevice1.getName()).thenReturn("device1");
        when(mockDevice2.getScreenHeight()).thenReturn(1000);
        when(mockDevice2.getName()).thenReturn("device2");
        when(mockDevice3.getScreenHeight()).thenReturn(1000);
        when(mockDevice3.getName()).thenReturn("device3");
        when(mockDevice4.getScreenHeight()).thenReturn(1000);
        when(mockDevice4.getName()).thenReturn("device4");
        when(mockDevice5.getScreenHeight()).thenReturn(1000);
        when(mockDevice5.getName()).thenReturn("device5");
    }

    @Test
    @DisplayName("Reads device specific configuration property according to device name")
    void readsDeviceSpecificConfigurationProperty() {
        AndroidTestDeviceConfiguration device1 = new AndroidTestDeviceConfiguration(mockDevice1);
        AndroidTestDeviceConfiguration device2 = new AndroidTestDeviceConfiguration(mockDevice2);

        assertEquals(10, device1.ANDROID_SCROLL_MAX_AMOUNT);
        assertEquals(3, device2.ANDROID_SCROLL_MAX_AMOUNT);
    }

    @Test
    @DisplayName("Uses default system values when configuration property for device is not defined and is not computed from device viewport")
    void usesDefaultSystemValuesWhenConfigurationPropertyNotDefined() {
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice0);

        assertEquals(AndroidTestDeviceConfiguration.ANDROID_DEFAULT_SCROLL_MAX_AMOUNT, device.ANDROID_SCROLL_MAX_AMOUNT);
    }

    @Test
    @DisplayName("Uses user defined upper status bar height when it is defined as property")
    void usesUserDefinedUpperStatusBarHeight() {
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice1);

        assertEquals(15, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

    @Test
    @DisplayName("Uses computed value as percent from device viewport when upper status bar property is not defined")
    void usesComputedUpperStatusBarHeight(){
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice2);

        assertEquals(40, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

    @Test
    @DisplayName("Ignores status bar adjustments when user defined upper status bar height is available as property")
    void ignoresStatusBarAdjustmentsWhenUserDefinedStatusBarHeightIsAvailable() {
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice1);

        assertEquals(15, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

    @Test
    @DisplayName("Adds user defined amount of pixels to computed status bar height when positive height adjustment property is defined")
    void addsUserDefinedPixelsWhenPositiveHeightAdjustmentIsDefined(){
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice3);

        assertEquals(43, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

    @Test
    @DisplayName("Subtracts user defined amount of pixels from computer status bar height when negative height adjustment property is defined")
    void removesUserDefinedPixelsWhenNegativeHeightAdjustmentIsDefined() {
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice4);

        assertEquals(36, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

    @Test
    @DisplayName("Uses positive height adjustment and ignores negative adjustment when both positive and negative height adjustment properties are defined")
    void usesOnlyPositiveHeightAdjustmentWhenBothAdjustmentsAreDefined() {
        AndroidTestDeviceConfiguration device = new AndroidTestDeviceConfiguration(mockDevice5);

        assertEquals(43, device.ANDROID_UPPER_STATUS_BAR_HEIGHT);
    }

}