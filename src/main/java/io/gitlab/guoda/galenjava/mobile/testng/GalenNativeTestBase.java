package io.gitlab.guoda.galenjava.mobile.testng;

import com.galenframework.testng.GalenTestNgTestBase;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

@Listeners(GalenFunctionalTestNgReportsListener.class)
public abstract class GalenNativeTestBase extends GalenTestNgTestBase {
    private static final Logger LOG = LoggerFactory.getLogger(GalenNativeTestBase.class);

    /**
     * Sets TestNG data provider of TestDevice devices to be used for tests.
     * See <a href="http://testng.org/">http://testng.org/</a> for more info on using data providers as parameters.
     * @return TestNG data provider object array
     */
    @DataProvider(name = "devices")
    public abstract Object [][] devices ();

    /**
     * Starts mobile drivers for all devices defined in 'devices' data provider.
     * Call from test as @BeforeClass or @BeforeSuite, according to tests context.
     * Do not forget to add (alwaysRun = true)!
     */
    public void startAllDeviceDrivers(Object[][] devicesDataProvider) {
        for (Object[] device : devicesDataProvider) {
            startDriverForDevice(device);
        }
    }

    /**
     * Stops mobile drivers started for devices defined in 'devices' data provider.
     * Call from test as @AfterClass or @AfterSuite, according to tests context and how drivers were started.
     * Do not forget to add (alwaysRun = true)!
     */
    public void stopAllDeviceDrivers(Object[][] devicesDataProvider) {
        for (Object[] device : devicesDataProvider) {
            stopDriverForDevice(device);
        }
    }

    public void stopDriverForDevice(Object[] deviceAsDataProviderObject) {
        TestDevice device = (TestDevice) deviceAsDataProviderObject[0];
        device.getDriver().quit();
    }

    public void startDriverForDevice(Object[] deviceAsDataProviderObject) {
        super.initDriver(deviceAsDataProviderObject);
        WebDriver driver = getDriver();
        TestDevice testDevice = getTestDevice(deviceAsDataProviderObject);
        initDevice(testDevice, driver);
    }

    /**
     *  Reloads mobile drivers instead of quitting / starting driver to improve native app performance and sets DeviceUnderTest.
     *  initDriver run as @BeforeMethod is required by original GalenFramework.
     * @param args
     */
    @BeforeMethod(alwaysRun = true)
    public void initDriver(Object[] args) {
        TestDevice testDevice = getTestDevice(args);
        LOG.debug("Init mobile driver for device " + testDevice.getName());

        DeviceUnderTest.set(testDevice);

        WebDriver deviceDriver = testDevice.getDriver();
        this.driver.set(deviceDriver);                         // set driver ThreadLocal from original Galen framework

        if (deviceDriver instanceof AndroidDriver) {
            LOG.debug("Reloading Android driver for device " + testDevice.getName());
            ((AndroidDriver)deviceDriver).resetApp();
        }
    }

    /**
     * Unsets DeviceUnderTest, does not quit mobile driver to improve native app performance.
     * quitDriver run as @AfterMethod is required by original GalenFramework.
     */
    @AfterMethod(alwaysRun = true)
    public void quitDriver() {
        LOG.debug("Overriding default Galen quitDriver to keep alive mobile drivers between tests");
        DeviceUnderTest.unset();
    }

    /**
     * Initializes configuration for test device.
     * @param device
     * @param driver
     */
    protected void initDevice(TestDevice device, WebDriver driver) {
        device.setDriver(driver);

        if (device.getScreenSize() == null) {
            device.setScreenSize(driver.manage().window().getSize());
        }
        device.loadConfiguration();
    }

    /**
     * Checks layout for specified page on specified device.
     * Uses Galen specification defined for page by {@link io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase#setPageGalenSpecPath(String)}.
     * Uses tags specified for device as included tags for layout checking.
     */
    public void checkLayout(GalenNativeTestPageBase page, TestDevice device) {
        page.checkLayout(device.getTags(), getReport());
    }

    /**
     * Gets TestDevice for current test from test arguments. Assumes device from DataProvider "devices" is passed as first argument to current test.
     * @param args array of test arguments
     * @return test device which is used to run current test instance
     * @throws IllegalArgumentException
     */
    protected TestDevice getTestDevice(Object[] args) throws IllegalArgumentException {
        if (args[0] instanceof TestDevice) {
            return (TestDevice) args[0];
        }

        throw new IllegalArgumentException("No Galen TestDevice found as first argument");
    }
}
