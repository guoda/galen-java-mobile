package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;

import java.util.Map;

public interface GalenLocator {
    void initGalenLocators(GalenNativeTestPageBase testPage);
    public Map<String, Locator> getGalenLocators();
    public Map<String, Locator> getGalenCheckImageLocators();
    Map<String, Locator> getGalenDumpImageLocators();
}
