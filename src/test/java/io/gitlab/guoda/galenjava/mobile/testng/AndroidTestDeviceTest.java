package io.gitlab.guoda.galenjava.mobile.testng;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.openqa.selenium.Dimension;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;

class AndroidTestDeviceTest {
    @Test
    void canInitDeviceByDeviceName(){
        final String DEVICE_NAME = "Device1";
        AndroidTestDevice target = new AndroidTestDevice(DEVICE_NAME);

        assertEquals(DEVICE_NAME, target.getName(), "Device name is other than expected");
        assertEquals(1, target.getTags().size(), "Tags list size is other than expected");
        assertEquals(DEVICE_NAME, target.getTags().get(0), "Tag value is other than expected");
        assertEquals(null, target.getScreenSize(), "Screen size is other than expected");
    }

    @Test
    void canInitDeviceByDeviceNameAndTags() {
        final String DEVICE_NAME = "Device1";
        final List<String> DEVICE_TAGS = asList("Tag1ForDevice", "otherTagForDevice");
        AndroidTestDevice target = new AndroidTestDevice(DEVICE_NAME, DEVICE_TAGS);

        assertEquals(DEVICE_NAME, target.getName(), "Device name is other than expected");
        assertEquals(2, target.getTags().size(), "Tags list size is other than expected");
        assertEquals(DEVICE_TAGS.get(0), target.getTags().get(0), "Tag-1 value is other than expected");
        assertEquals(DEVICE_TAGS.get(1), target.getTags().get(1), "Tag-2 value is other than expected");
        assertEquals(null, target.getScreenSize(), "Screen size is other than expected");
    }

    @Test
    void canInitDeviceByNameTagsAndScreenSize() {
        final String DEVICE_NAME = "Device1";
        final List<String> DEVICE_TAGS = asList("Tag1ForDevice", "otherTagForDevice");
        final Dimension SCREEN_SIZE = new Dimension(800, 1024);

        AndroidTestDevice target = new AndroidTestDevice(DEVICE_NAME, SCREEN_SIZE, DEVICE_TAGS);

        assertEquals(DEVICE_NAME, target.getName(), "Device name is other than expected");
        assertEquals(2, target.getTags().size(), "Tags list size is other than expected");
        assertEquals(DEVICE_TAGS.get(0), target.getTags().get(0), "Tag-1 value is other than expected");
        assertEquals(DEVICE_TAGS.get(1), target.getTags().get(1), "Tag-2 value is other than expected");
        assertEquals(SCREEN_SIZE, target.getScreenSize(), "Screen size is other than expected");
    }

    @Test
    @DisplayName("Returns string 'android' as device type")
    void returnsAndroidAsDeviceType() {
        AndroidTestDevice target = new AndroidTestDevice("Device1");

        assertEquals("android", target.getDeviceType());
    }

    @Test
    @DisplayName("Returns default Appium system port capability value as 8200 when value is not set when creating device")
    void returnsDefaultSystemPortWhenNotSet() {
        AndroidTestDevice target = new AndroidTestDevice("Device1");

        assertEquals(8200, target.getAppiumSystemPort());
    }

    @Test
    void canSetValuesForAppiumCapabilitiesInAnyOrder() throws MalformedURLException {
        AndroidTestDevice device1 = new AndroidTestDevice("Device1");
        AndroidTestDevice device2 = new AndroidTestDevice("Device2");

        device1
                .setAppiumDeviceName("DD000001")
                .setAppiumSystemPort(9001)
                .setUdid("DD000001")
                .setAppiumServerBaseUrl("http://127.0.0.1:4723/wd/hub");
        device2
                .setUdid("DD000002")
                .setAppiumServerBaseUrl("http://127.0.0.1:4725/wd/hub")
                .setAppiumSystemPort(9002)
                .setAppiumDeviceName("DD000002");

        assertEquals("DD000001", device1.getAppiumDeviceName(), "Device1 name is other than expected");
        assertEquals("DD000001", device1.getUdid(), "Device1 udid is other than expected");
        assertEquals(9001, device1.getAppiumSystemPort(), "Device1 system port is other than expected");
        assertEquals(new URL("http://127.0.0.1:4723/wd/hub"), device1.getAppiumServerBaseUrl(), "Device1 URL is other than expected");

        assertEquals("DD000002", device2.getAppiumDeviceName(), "Device2 name is other than expected");
        assertEquals("DD000002", device2.getUdid(), "Device2 udid is other than expected");
        assertEquals(9002, device2.getAppiumSystemPort(), "Device2 system port is other than expected");
        assertEquals(new URL("http://127.0.0.1:4725/wd/hub"), device2.getAppiumServerBaseUrl(), "Device2 URL is other than expected");
    }
}