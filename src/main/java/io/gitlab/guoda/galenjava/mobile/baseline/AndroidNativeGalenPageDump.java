package io.gitlab.guoda.galenjava.mobile.baseline;

import com.galenframework.api.PageDump;
import com.galenframework.browser.Browser;
import com.galenframework.page.PageElement;
import com.galenframework.specs.page.PageSpec;
import com.galenframework.validation.PageValidation;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.browser.AndroidBrowser;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;


public class AndroidNativeGalenPageDump extends DefaultNativeGalenPageDump {
    private AndroidBrowser browser;

    public AndroidNativeGalenPageDump(String pageName, AndroidDriver driver) {
        super(pageName);
        browser = new AndroidBrowser(driver);
        setBrowser(browser);
    }

    @Override
    public void dumpPage(Browser browser, PageSpec pageSpec, String reportFolderPath) throws IOException {
        File reportFolder = createReportFolder(reportFolderPath);

        PageValidation pageValidation = new PageValidation(browser, browser.getPage(), pageSpec, null, null);

        List<String> excludedObjects = getExcludedObjects();
        List<Pattern> patterns = convertPatterns(excludedObjects);

        Set<String> finalObjectNames = getFinalObjectNames(pageSpec);

        PageDump pageDump = new PageDump();
        pageDump.setTitle(browser.getPage().getTitle());

        exportAllScreenshots(pageDump, finalObjectNames, patterns, pageValidation, reportFolder);

        if(!isOnlyImages()) {
            createDumpReport(pageDump, reportFolder);
        }
    }

    public void exportAllScreenshots(PageDump pageDump, Set<String> objectNames, List<Pattern> excludedObjectNamePatterns, PageValidation pageValidation, File reportFolder) throws IOException {
        File objectsFolder = createObjectsFolder(reportFolder);

        for (String objectName : objectNames) {
            if (!elementShouldBeExcluded(objectName, excludedObjectNamePatterns)) {
                PageElement pageElement = pageValidation.findPageElement(objectName);

                if (canScreenshotElement(pageElement)) {
                    takeElementScreenshot(browser, pageElement, objectName, reportFolder, objectsFolder);
                    addElementToPageDump(pageDump, pageElement, objectName, true);
                }
            }
        }
    }

    public void addElementToPageDump(PageDump pageDump, PageElement pageElement, String objectName, boolean hasImage) {
        PageDump.Element element = new PageDump.Element(objectName, pageElement.getArea().toIntArray());
        element.setHasImage(hasImage);
        pageDump.addElement(element);
    }
}
