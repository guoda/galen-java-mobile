package io.gitlab.guoda.galenjava.mobile.testpage;

import com.galenframework.page.PageElement;
import com.galenframework.specs.page.Locator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenDefaultNativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.page.NativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.scrollable.GalenNativeScrollable;
import org.openqa.selenium.WebDriver;

/**
 * Use as base class for describing native mobile Page Object for scrollable pages.
 */
public abstract class GalenNativeScrollableTestPageBase extends GalenNativeTestPageBase implements GalenNativeScrollable {
    private NativeScrollablePage page;

    public GalenNativeScrollableTestPageBase(WebDriver driver) {
        super(driver);
    }

    public void initPage(WebDriver driver) {
        if (driver instanceof AndroidDriver) {
            page = new GalenAndroidNativeScrollablePage((AndroidDriver) driver);
        } else {
            page = new GalenDefaultNativeScrollablePage(driver);
        }
        super.setPage(page);
    }

    @Override
    public void scrollToElement(MobileElement mobileElement) {
        page.scrollToElement(mobileElement);
    }

    @Override
    public void scrollToElement(MobileElement mobileElement, int maxScrolls) {
        page.scrollToElement(mobileElement, maxScrolls);
    }

    @Override
    public PageElement scrollToElement(Locator locator, int maxScrolls) {
        GalenNativeScrollable galenPage = (GalenNativeScrollable) page;
        return galenPage.scrollToElement(locator, maxScrolls);
    }

    @Override
    public void setPageTopElement(MobileElement pageTopElement) {
        page.setPageTopElement(pageTopElement);
    }

    @Override
    public void setPageBottomElement(MobileElement pageBottomElement) {
        page.setPageBottomElement(pageBottomElement);
    }

}
