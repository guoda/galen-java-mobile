package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.PageElement;
import com.galenframework.specs.page.Locator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.scrollable.GalenAndroidNativeScrollable;
import io.gitlab.guoda.galenjava.mobile.scrollable.GalenNativeScrollable;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDeviceConfiguration;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GalenAndroidNativeScrollablePage extends GalenAndroidNativePage implements NativeScrollablePage, GalenNativeScrollable {
    private static final Logger LOG = LoggerFactory.getLogger(GalenAndroidNativeScrollablePage.class);
    GalenAndroidNativeScrollable scroller;

    public GalenAndroidNativeScrollablePage(AndroidDriver driver) {
        super(driver);
        scroller = new GalenAndroidNativeScrollable(driver);
    }

    /**
     * Adds custom scrolling for Android native apps according to device configuration parameters,
     * as Selenium default 'getObject' from original Galen Framework does not scroll Android native.
     * @param locator Galen locator for element
     * @return
     */
    @Override
    public PageElement getObject(Locator locator) {
        AndroidTestDeviceConfiguration CONFIG = (AndroidTestDeviceConfiguration) DeviceUnderTest.get().getConfiguration();
        LOG.debug("Initializing scroll for device " + DeviceUnderTest.get().getName() + " with max scrolls " + CONFIG.ANDROID_SCROLL_MAX_AMOUNT);

        return scroller.scrollToElement(locator, CONFIG.ANDROID_SCROLL_MAX_AMOUNT);
    }

    /**
     * Scrolls to MobileElement on Android device.
     * @see GalenAndroidNativeScrollable#scrollToElement(MobileElement) for details.
     * @param mobileElement
     */
    @Override
    public void scrollToElement(MobileElement mobileElement) {
        scroller.scrollToElement(mobileElement);
    }

    /**
     * Scrolls to MobileElement on Android device using predefined max amount of scrolls.
     * @see GalenAndroidNativeScrollable#scrollToElement(MobileElement, int) for details.
     * @param mobileElement
     * @param maxScrolls
     */
    @Override
    public void scrollToElement(MobileElement mobileElement, int maxScrolls) {
        scroller.scrollToElement(mobileElement, maxScrolls);
    }

    /**
     * Sets page top element for Android native page.
     * @param pageTopElement
     */
    @Override
    public void setPageTopElement(MobileElement pageTopElement) {
        scroller.setPageTopElement(pageTopElement);
    }

    /**
     * Sets page bottom element for Android native page.
     * @param pageBottomElement
     */
    @Override
    public void setPageBottomElement(MobileElement pageBottomElement) {
        scroller.setPageBottomElement(pageBottomElement);
    }

    /**
     * Scrolls to mobile element which is identified by Galen locator using predefined max amount of scrolls.
     * @see GalenAndroidNativeScrollable#scrollToElement(Locator, int) for details.
     * @param locator Galen locator for element
     * @param maxScrolls
     * @return
     */
    @Override
    public PageElement scrollToElement(Locator locator, int maxScrolls) {
        return scroller.scrollToElement(locator, maxScrolls);
    }
}
