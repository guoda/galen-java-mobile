package io.gitlab.guoda.galenjava.mobile.locator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Used with Appium Page Object Factory to mark MobileElement page object locator which is used for visual validation.
 * Affects doing baseline images:
 * - when at least a single mobile element in test page object have this annotation, Galen page dump will take screenshots of only annotated page elements when doing baseline.
 * - when none of mobile elements in test page object have this annotation, Galen page dump will take screenshots of all page elements when doing baseline.
 */
@Retention(RUNTIME) @Target({FIELD, TYPE})
public @interface GalenCheckImage {
}
