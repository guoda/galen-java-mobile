package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.PageElement;

import java.awt.image.BufferedImage;
import java.io.File;

public interface GalenNativePage extends NativePage {
    File getScreenshotFile();
    BufferedImage getScreenshotImage();
    PageElement getSpecialObject(String objectName);
}
