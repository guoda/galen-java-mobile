package io.gitlab.guoda.galenjava.mobile.scrollable;

import com.galenframework.page.AbsentPageElement;
import com.galenframework.page.PageElement;
import com.galenframework.page.selenium.SeleniumPage;
import com.galenframework.specs.page.Locator;
import io.appium.java_client.android.AndroidDriver;

public class GalenAndroidNativeScrollable extends AndroidNativeScrollable implements GalenNativeScrollable {
    SeleniumPage seleniumPageMethodAccessor;

    public GalenAndroidNativeScrollable(AndroidDriver driver) {
        super(driver);
        seleniumPageMethodAccessor = new SeleniumPage(driver);
    }

    /**
     * Scrolls to MobileElement on Android device under test using max amount of scrolls.
     * Same amount of scrolls is used for scrolling both directions. E.g., when max amount of scrolls is 5:
     * - scroll down 5 times or until bottom element is reached (if defined). If element not found:
     * - scroll up another 5 times or until top element is reached (if defined).
     * When page top or/and page bottom element is not defined, scrolling is always performed {@code maxScrolls} times for both directions.
     * @param locator Galen Locator used to identify element
     * @param maxScrolls
     * @return Galen page element
     */
    @Override
    public PageElement scrollToElement(Locator locator, int maxScrolls) {
        PageElement pageElement;

        try {
            pageElement = scrollDownToElement(locator, maxScrolls);
        } catch (NoSuchElementScrollLimitReachedException e) {
            pageElement = scrollUpToElement(locator, maxScrolls);
        }

        return pageElement;
    }

    private PageElement scrollDownToElement(Locator locator, int maxScrolls) {
        PageElement pageElement = getObject(locator);

        if (!isElementAbsent(pageElement)) {
            return pageElement;
        }

        if (maxScrolls == 0) {
            throw new NoSuchElementScrollLimitReachedException("Amount of max scrolls down exceeded, element not found using Galen element search");
        }

        scrollDown();
        return scrollDownToElement(locator, maxScrolls - 1);
    }

    private PageElement scrollUpToElement(Locator locator, int maxScrolls) {
        PageElement pageElement = getObject(locator);

        if (!isElementAbsent(pageElement)) {
            return pageElement;
        }

        if (maxScrolls == 0) {
            throw new NoSuchElementScrollLimitReachedException("Amount of max scrolls up exceeded, element not found using Galen element search");
        }

        scrollUp();
        return scrollUpToElement(locator, maxScrolls - 1);
    }

    private PageElement getObject(Locator locator) {
        return seleniumPageMethodAccessor.getObject(locator);
    }

    /*
        Original Galen Framework returns AbsentPageElement when element is not found on page using Galen Locator; no error thrown.
     */
    private boolean isElementAbsent(PageElement element) {
        return element.getClass() == AbsentPageElement.class;
    }
}
