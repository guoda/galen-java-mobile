package io.gitlab.guoda.galenjava.mobile.testpage;

import com.galenframework.browser.Browser;
import com.galenframework.browser.SeleniumBrowser;
import com.galenframework.reports.TestReport;
import com.galenframework.specs.page.Locator;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.baseline.AndroidNativeGalenPageDump;
import io.gitlab.guoda.galenjava.mobile.baseline.DefaultNativeGalenPageDump;
import io.gitlab.guoda.galenjava.mobile.baseline.GalenPageDumpNative;
import io.gitlab.guoda.galenjava.mobile.browser.AndroidBrowser;
import io.gitlab.guoda.galenjava.mobile.core.GalenLayoutNativeAsserter;
import io.gitlab.guoda.galenjava.mobile.locator.AndroidNativeGalenLocator;
import io.gitlab.guoda.galenjava.mobile.locator.DefaultGalenLocator;
import io.gitlab.guoda.galenjava.mobile.locator.GalenLocator;
import io.gitlab.guoda.galenjava.mobile.page.DefaultNativePage;
import io.gitlab.guoda.galenjava.mobile.page.NativePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenNativePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativePage;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Use as base class for describing native mobile Page Object for non-scrollable pages.
 */
public abstract class GalenNativeTestPageBase {
    private static final Logger LOG = LoggerFactory.getLogger(GalenNativeTestPageBase.class);

    private WebDriver driver;
    private NativePage page;
    private GalenLocator locator;
    private String pageScreenshotsBaselinePath;
    private String pageGalenSpecPath;

    public GalenNativeTestPageBase(WebDriver driver) {
        initPage(driver);
        this.driver = driver;
        initGalenLocators();
    }

    public void initPage(WebDriver driver) {
        if (driver instanceof AndroidDriver) {
            page = new GalenAndroidNativePage((AndroidDriver) driver);
        } else {
            page = new DefaultNativePage(driver);
        }
    }

    public void initGalenLocators() {
        if (driver instanceof AndroidDriver) {
            locator = new AndroidNativeGalenLocator();
        } else {
            locator = new DefaultGalenLocator();
        }
        locator.initGalenLocators(this);
    }

    protected void setPage(NativePage page) {
        this.page = page;
    }

    protected NativePage getPage() {
        return page;
    }

    public Map<String, Locator> getGalenLocators() {
        return locator.getGalenLocators();
    }

    public Map<String, Locator> getGalenCheckImageLocators() {
        return locator.getGalenCheckImageLocators();
    }

    //TODO: explore option to add this as variable(+"objects" dir) for usage in GalenSpecs to avoid duplication
    public void setPageScreenshotsBaselinePath(String deviceName) {
        String pageSpecPath = getPageGalenSpecPath();
        String basePath = new File("").getAbsolutePath();
        String baselinePath = basePath + File.separator + TestPageBaseConfiguration.getBaselineRoot();
        String pageDumpDir = StringUtils.substringAfterLast(pageSpecPath, "/").split(".spec")[0];
        pageScreenshotsBaselinePath = baselinePath + File.separator + deviceName + File.separator + pageDumpDir;
    }

    public String getPageGalenSpecPath() {
        return pageGalenSpecPath;
    }

    public void setPageGalenSpecPath(String pageGalenSpecPath) {
        this.pageGalenSpecPath = pageGalenSpecPath;
    }

    public Map<String, Locator> getGalenDumpImageLocators() {
        return locator.getGalenDumpImageLocators();
    }

    public void dumpPageImages() {
        //Page name is not visible when dumping only images, no need to set in this case to anything human readable
        String pageName = "";

        setPageScreenshotsBaselinePath(DeviceUnderTest.get().toFilePath());

        GalenPageDumpNative pageDumper = initPageDumper(pageName);
        pageDumper.dumpPageImages(getPageScreenshotsBaselinePath(), getGalenDumpImageLocators());
    }

    public GalenPageDumpNative initPageDumper(String pageName) {
        GalenPageDumpNative pageDumper;

        if (driver instanceof AndroidDriver) {
            pageDumper = new AndroidNativeGalenPageDump(pageName, (AndroidDriver) driver);
        } else {
            pageDumper = new DefaultNativeGalenPageDump(pageName);
        }

        return pageDumper;
    }

    public String getPageScreenshotsBaselinePath() {
        return pageScreenshotsBaselinePath;
    }

    public void checkLayout(List<String> includedTags, TestReport report) {
        if (page instanceof GalenNativePage) {
            try {
                checkCurrentPageLayout(includedTags, report);
            } catch (IOException e) {
                throw new RuntimeException("Error while checking page layout", e);
            }
        } else {
            throw new RuntimeException("checkLayout call is valid only for GalenNativePage kind of pages");
        }
    }

    protected void checkCurrentPageLayout(List<String> includedTags, TestReport report) throws IOException {
        GalenLayoutNativeAsserter layoutAsserter = new GalenLayoutNativeAsserter(getBrowser(driver), report);
        layoutAsserter.checkLayout(getPageGalenSpecPath(), includedTags, getGalenLocators());
    }

    protected Browser getBrowser(WebDriver driver) {
        if (driver instanceof AndroidDriver) {
            LOG.debug("Initializing Android browser...");
            return new AndroidBrowser((AndroidDriver) driver, this);
        }

        LOG.debug("Initializing Selenium browser...");
        return new SeleniumBrowser(driver);
    }

}
