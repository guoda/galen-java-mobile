package io.gitlab.guoda.galenjava.mobile.testpage;

import io.gitlab.guoda.galenjava.mobile.utils.GalenJavaMobileConfiguration;

public class TestPageBaseConfiguration extends GalenJavaMobileConfiguration {
    private static String BASELINE_ROOT_KEY = SYSTEM_PREFIX + "baselineRoot";

    /**
     * Gets root location for storing baseline images. Relative to $PROJECT_ROOT, defaults to 'baseline'.
     * @return
     */
    public static String getBaselineRoot() {
        return properties.getString(BASELINE_ROOT_KEY, "baseline");
    }
}
