package io.gitlab.guoda.galenjava.mobile.scrollable;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDevice;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDeviceConfiguration;
import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;

import org.openqa.selenium.NoSuchElementException;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class AndroidNativeScrollableTest {
    private static AndroidDriver driver = mock(AndroidDriver.class, RETURNS_DEEP_STUBS);

    @BeforeAll
    static void initMockTest() {
        TestDevice device = new AndroidTestDevice("DummyTestDevice", new Dimension(800, 1000), asList("anyTag"));
        device.loadConfiguration();
        device.setDriver(driver);
        DeviceUnderTest.set(device);
    }

    @AfterAll
    static void teardownMockTest() {
        DeviceUnderTest.unset();
    }

    @Test
    @DisplayName("Throws error when max scroll amount is reached and element is not displayed")
    void throwsErrorWhenMaxScrollAmountIsReachedAndElementNotDisplayed() {
        AndroidNativeScrollable target = new AndroidNativeScrollable(driver);
        AndroidNativeScrollable targetSpy = spy(target);

        MobileElement element = mock(AndroidElement.class);
        when(element.isDisplayed())
                .thenReturn(false);

        assertThrows(NoSuchElementException.class, () -> {
            targetSpy.scrollToElement(element,3);
        });

        verify(targetSpy, times(3)).scrollDown();
        verify(targetSpy, times(0)).scrollUp();
    }

    @Test
    @DisplayName("Stops scrolling when element is displayed in viewport and max scroll amount is not reached")
    void stopsScrollingWhenElementIsDisplayedAndMaxScrollAmountNotReached() {
        AndroidNativeScrollable target = new AndroidNativeScrollable(driver);
        AndroidNativeScrollable targetSpy = spy(target);

        MobileElement element = mock(AndroidElement.class);
        when(element.isDisplayed())
                .thenReturn(false)
                .thenReturn(true);

        targetSpy.scrollToElement(element,3);
        verify(targetSpy, times(2)).scrollDown();
        verify(targetSpy, times(0)).scrollUp();
    }

    @Test
    @DisplayName("Starts scrolling up when page bottom element is displayed in viewport, max scroll amount is not reached and expected element not found")
    void startsScrollingUpWhenPageBottomElementIsReachedAndMaxScrollAmountNotReached() {
        AndroidNativeScrollable target = new AndroidNativeScrollable(driver);
        AndroidNativeScrollable targetSpy = spy(target);

        MobileElement element = mock(AndroidElement.class);
        when(element.isDisplayed())
                .thenThrow(NoSuchElementException.class)
                .thenReturn(false)
                .thenReturn(false)
                .thenReturn(false)
                .thenReturn(true);

        MobileElement pageBottomElement = mock(AndroidElement.class);
        when(pageBottomElement.isDisplayed())
                .thenReturn(false)
                .thenReturn(true);

        targetSpy.setPageBottomElement(pageBottomElement);

        targetSpy.scrollToElement(element,5);
        verify(targetSpy, times(1)).scrollDown();
        verify(targetSpy, times(4)).scrollUp();
    }

    @Test
    @DisplayName("Throws error when page top element is reached while scrolling up and expected element not found")
    void throwsErrorWhenPageTopElementReachedAndExpectedElementNotFound() {
        AndroidNativeScrollable target = new AndroidNativeScrollable(driver);
        AndroidNativeScrollable targetSpy = spy(target);

        MobileElement element = mock(AndroidElement.class);
        when(element.isDisplayed())
                .thenReturn(false);

        MobileElement pageBottomElement = mock(AndroidElement.class);
        when(pageBottomElement.isDisplayed())
                .thenThrow(NoSuchElementException.class)
                .thenReturn(true);

        MobileElement pageTopElement = mock(AndroidElement.class);
        when(pageTopElement.isDisplayed())
                .thenThrow(NoSuchElementException.class)
                .thenReturn(false)
                .thenReturn(true);

        targetSpy.setPageBottomElement(pageBottomElement);
        targetSpy.setPageTopElement(pageTopElement);

        assertThrows(NoSuchElementScrollLimitReachedException.class, () -> {
            targetSpy.scrollToElement(element,5);
        });
        verify(targetSpy, times(1)).scrollDown();
        verify(targetSpy, times(2)).scrollUp();
    }

    @Test
    @DisplayName("Throws error when max scroll amount is reached while scrolling up")
    void throwsErrorWhenMaxScrollAmountReachedScrollingUp() {
        AndroidNativeScrollable target = new AndroidNativeScrollable(driver);
        AndroidNativeScrollable targetSpy = spy(target);

        MobileElement element = mock(AndroidElement.class);
        when(element.isDisplayed())
                .thenThrow(NoSuchElementException.class)
                .thenReturn(false);

        MobileElement pageBottomElement = mock(AndroidElement.class);
        when(pageBottomElement.isDisplayed())
                .thenReturn(false)
                .thenReturn(true);

        MobileElement pageTopElement = mock(AndroidElement.class);
        when(pageTopElement.isDisplayed())
                .thenReturn(false);

        targetSpy.setPageBottomElement(pageBottomElement);
        targetSpy.setPageTopElement(pageTopElement);

        assertThrows(NoSuchElementException.class, () -> {
            targetSpy.scrollToElement(element,5);
        });
        verify(targetSpy, times(1)).scrollDown();
        verify(targetSpy, times(5)).scrollUp();
    }
}