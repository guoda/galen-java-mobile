package io.gitlab.guoda.galenjava.mobile.scrollable;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Dimension;

import java.time.Duration;

public abstract class AndroidScrollable {

    /**
     * Scrolls down Android native page one time. Will use wait of 500 milliseconds before next scroll, if defined.
     * @param driver
     */
    public static void scrollDown(AndroidDriver driver) {
        Dimension windowSize = driver.manage().window().getSize();
        scrollDown(driver, windowSize, 500);
    }

    /**
     * Scrolls down Android native page one time. Uses {@ref windowSize} to calculate start and end position of scroll as percentage of windowSize.
     * @param driver
     * @param windowSize size of window which will be scrolled
     * @param waitDurationMillis duration in milliseconds to wait for scroll to complete
     */
    public static void scrollDown(AndroidDriver driver, Dimension windowSize, long waitDurationMillis) {
        int startX = (int) ((windowSize.getWidth()) * 0.80);
        int endX = (int) ((windowSize.getWidth()) * 0.20);

        int startY = (int) ((windowSize.getHeight()) * 0.80);
        int endY = (int) ((windowSize.getHeight()) * 0.20);

        scrollVertical(driver, startX, startY, endX, endY, waitDurationMillis);
    }

    /**
     * Scrolls up Android native page one time. Will use wait of 500 milliseconds before next scroll, if defined.
     * @param driver
     */
    public static void scrollUp(AndroidDriver driver) {
        Dimension windowSize = driver.manage().window().getSize();
        scrollUp(driver, windowSize,500);
    }

    /**
     * Scrolls up Android native page one time. Uses {@ref windowSize} to calculate start and end position of scroll as percentage of windowSize.
     * @param driver
     * @param windowSize size of window which will be scrolled
     * @param waitDurationMillis duration in milliseconds to wait for scroll to complete
     */
    public static void scrollUp(AndroidDriver driver, Dimension windowSize, long waitDurationMillis) {
        int startX = (int) ((windowSize.getWidth()) * 0.20);
        int endX = (int) ((windowSize.getWidth()) * 0.80);

        int startY = (int) ((windowSize.getHeight()) * 0.20);
        int endY = (int) ((windowSize.getHeight()) * 0.80);

        scrollVertical(driver, startX, startY, endX, endY, waitDurationMillis);
    }

    private static void scrollVertical(AndroidDriver driver, int startX, int startY, int endX, int endY, long waitDurationMillis) {
        TouchAction actions = new TouchAction(driver);
        actions.press(startX, startY).waitAction(Duration.ofMillis(waitDurationMillis)).moveTo(endX, endY).release().perform();
    }
}
