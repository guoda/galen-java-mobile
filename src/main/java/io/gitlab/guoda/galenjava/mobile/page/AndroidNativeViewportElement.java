package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.Rect;
import com.galenframework.page.selenium.ViewportElement;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

public class AndroidNativeViewportElement extends ViewportElement {

    public AndroidNativeViewportElement(WebDriver driver) {
        super(driver);
    }

    @Override
    public Rect calculateArea() {
        Dimension viewportArea = getViewportArea();
        return new Rect(0, 0, viewportArea.width, viewportArea.height);
    }

    /*
        Returns the "usable" portion of Android device screen which:
         - excludes the region at the bottom where the back, home, and "show running apps" buttons are
         - includes upper status bar (charging, wireless etc indicators)
     */
    private Dimension getViewportArea() {
        return DeviceUnderTest.get().getScreenSize();
    }
}
