package io.gitlab.guoda.galenjava.mobile.utils;

import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;

/**
 * Device currently under test. Must be initialized for each test instance when running tests in parallel using different devices.
 */
public class DeviceUnderTest {
    public static final ThreadLocal<TestDevice> deviceUnderTest = new ThreadLocal();

    public static void set(TestDevice device) {
        deviceUnderTest.set(device);
    }

    public static TestDevice get() {
        return deviceUnderTest.get();
    }

    public static void unset() {
        deviceUnderTest.remove();
    }
}
