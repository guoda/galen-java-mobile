package io.gitlab.guoda.galenjava.mobile.scrollable;

import org.openqa.selenium.NotFoundException;

public class NoSuchElementScrollLimitReachedException extends NotFoundException {
    public NoSuchElementScrollLimitReachedException(String reason) {
        super(reason);
    }

    public NoSuchElementScrollLimitReachedException(String reason, Throwable cause) {
        super(reason, cause);
    }
}
