package io.gitlab.guoda.galenjava.mobile.browser;

import com.galenframework.browser.SeleniumBrowser;
import com.galenframework.page.Page;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeScrollableTestPageBase;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AndroidBrowser extends SeleniumBrowser {
    private static final Logger LOG = LoggerFactory.getLogger(AndroidBrowser.class);

    GalenNativeTestPageBase testPage = null;

    public AndroidBrowser(AndroidDriver driver) {
        super(driver);
    }

    public AndroidBrowser(AndroidDriver driver, GalenNativeTestPageBase testPage) {
        super(driver);
        this.testPage = testPage;
    }

    public void setTestPage(GalenNativeTestPageBase testPage) {
        this.testPage = testPage;
    }

    /**
     * Used as page method accessor. E.g. for taking screenshots, getting page title or performing other similar common actions,
     * which are not related to detailed structure of page under test.
     * @return new page for current device under test. Page type is:
     * - GalenAndroidNativePage for GalenNativeTestPageBase test page
     * - GalenAndroidNativeScrollablePage for GalenNativeScrollableTestPageBase test page or when test page is undefined
     */
    @Override
    public Page getPage() {
        if (testPage == null) {
            return getScrollablePage();
        }

        if (testPage instanceof GalenNativeScrollableTestPageBase) {
            return getScrollablePage();
        }

        LOG.debug("Initializing GalenAndroidNativePage...");
        return new GalenAndroidNativePage((AndroidDriver) DeviceUnderTest.get().getDriver());
    }

    private Page getScrollablePage() {
        LOG.debug("Initializing GalenAndroidNativeScrollablePage...");
        return new GalenAndroidNativeScrollablePage((AndroidDriver) DeviceUnderTest.get().getDriver());
    }
}
