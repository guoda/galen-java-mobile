package io.gitlab.guoda.galenjava.mobile.testng;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;

import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

public class DummyTestBaseImpl extends GalenNativeTestBase {

    @Override
    public WebDriver createDriver(Object[] args) {
        WebDriver driver = null;
        TestDevice device = (TestDevice) args[0];
        String deviceType = device.getDeviceType();

        switch (deviceType) {
            case "android":
                driver = mock(AndroidDriver.class, RETURNS_DEEP_STUBS);
                break;
            default:
                // exception is not thrown for dummyTestBaseImpl in order to be sure cases where user forgot to manage unknown driver types are caught
//                throw new IllegalArgumentException("Device type unknown");
        }

        return driver;
    }

    @DataProvider(name = "devices")
    public Object [][] devices () {
        return new Object[][] {{}};
    }
}
