package io.gitlab.guoda.galenjava.mobile.baseline;

import com.galenframework.api.GalenPageDump;
import com.galenframework.api.PageDump;
import com.galenframework.browser.Browser;
import com.galenframework.page.PageElement;
import com.galenframework.rainbow4j.Rainbow4J;
import com.galenframework.specs.page.Locator;
import com.galenframework.specs.page.PageSpec;
import io.gitlab.guoda.galenjava.mobile.locator.GalenFindBy;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Creates baseline image page dump for Galen visual tests.
 * @see GalenFindBy for explanation how elements to be dumped are selected
 * Dump of nativeViewport is always taken. Note that nativeViewport can be used for visual tests only for single screen application pages.
 *
 * Most methods are extracted from Galen Framework galen-core or implemented as access to local Galen Framework methods
 */
public class DefaultNativeGalenPageDump extends GalenPageDump implements GalenPageDumpNative {
    private final static Logger LOG = LoggerFactory.getLogger(DefaultNativeGalenPageDump.class);
    private Browser browser;

    private Integer maxWidth = null;
    private Integer maxHeight = null;

    public DefaultNativeGalenPageDump(String pageName) {
        super(pageName);
    }

    public void setBrowser(Browser browser) {
        this.browser = browser;
    }

    public void dumpPageImages(String pageBaselinePath, Map<String, Locator> galenDumpImageLocators) {
        dumpPage(true, pageBaselinePath, galenDumpImageLocators);
    }

    public void dumpPage(boolean dumpOnlyImages, String pageBaselinePath, Map<String, Locator> galenDumpImageLocators) {
        setOnlyImages(dumpOnlyImages);

        dumpPage(pageBaselinePath, galenDumpImageLocators);
    }

    public void dumpPage(String reportFolderPath, Map<String, Locator> galenLocators) {
        try {
            PageSpec pageSpec = new PageSpec(galenLocators);
            dumpPage(browser, pageSpec, reportFolderPath);
        } catch (IOException e) {
            throw new RuntimeException("Cannot create dump", e);
        }
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:dumpPage
    protected File createReportFolder(String reportFolderPath) {
        File reportFolder = new File(reportFolderPath);
        if (!reportFolder.exists()) {
            if (!reportFolder.mkdirs()) {
                throw new RuntimeException("Cannot create dir: " + reportFolder.getAbsolutePath());
            }
        }

        return reportFolder;
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:dumpPage
    // Modified finalObjectNames: removed "screen" objects
    protected Set<String> getFinalObjectNames(PageSpec pageSpec) {
        Set<String> objectNames = pageSpec.getObjects().keySet();

        Set<String> finalObjectNames = new HashSet<>();
        finalObjectNames.addAll(objectNames);
        finalObjectNames.add("viewport");

        return finalObjectNames;
    }

    // Use original private method from Galen Framework: galen-core -> api/GalenPageDump.java
    protected List<Pattern> convertPatterns(List<String> excludedObjects) {
        try {
            Method convertPatternsMethod = GalenPageDump.class.getDeclaredMethod("convertPatterns", List.class);
            convertPatternsMethod.setAccessible(true);
            Object patternsValue = convertPatternsMethod.invoke(this, excludedObjects);
            return (List<Pattern>) patternsValue;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOG.error("Error calling Galen private convertPatterns", e);
            return null;
        }
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:dumpPage
    protected boolean elementShouldBeExcluded(String objectName, List<Pattern> excludedObjectNamePatterns) {
        return matchesExcludedPatterns(objectName, excludedObjectNamePatterns);
    }

    // Use original private method from Galen Framework: galen-core -> api/GalenPageDump.java
    protected boolean matchesExcludedPatterns(String objectName, List<Pattern> patterns) {
        try {
            Method declaredMethod = GalenPageDump.class.getDeclaredMethod("matchesExcludedPatterns", String.class, List.class);
            declaredMethod.setAccessible(true);
            Object methodValue = declaredMethod.invoke(this, objectName, patterns);
            return (boolean) methodValue;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOG.error("Error calling Galen private matchesExcludedPatterns", e);
            return false;
        }
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java
    protected boolean canScreenshotElement(PageElement pageElement) {
        //TODO: review isVisible / isPresent and update for MobileElement
        if(!pageElement.isVisible()) {
            return false;
        }

        if (pageElement.getArea() == null) {
            return false;
        }

        if (!pageElement.isPresent()) {
            return false;
        }

        if(!isWithinArea(pageElement, maxWidth, maxHeight)) {
            return false;
        }

        return true;
    }

    // Use original private method from Galen Framework: galen-core -> api/GalenPageDump.java
    protected boolean isWithinArea(PageElement element, Integer maxWidth, Integer maxHeight) {
        try {
            Method declaredMethod = GalenPageDump.class.getDeclaredMethod("isWithinArea", PageElement.class, Integer.class, Integer.class);
            declaredMethod.setAccessible(true);
            Object methodValue = declaredMethod.invoke(this, element, maxWidth, maxHeight);
            return (boolean) methodValue;
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOG.error("Error calling Galen private isWithinArea", e);
            return false;
        }
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:dumpPage
    protected void createDumpReport(PageDump pageDump, File reportFolder) throws IOException {
        String pageName = getPageName();
        pageDump.setPageName(pageName);
        exportAsJson(pageDump, new File(reportFolder.getAbsoluteFile() + File.separator + "page.json"));
        exportAsHtml(pageDump, pageName, new File(reportFolder.getAbsoluteFile() + File.separator + "page.html"));
        copyResource("/html-report/jquery-1.11.2.min.js", new File(reportFolder.getAbsolutePath() + File.separator + "jquery-1.11.2.min.js"));
        copyResource("/pagedump/galen-pagedump.js", new File(reportFolder.getAbsolutePath() + File.separator + "galen-pagedump.js"));
        copyResource("/pagedump/galen-pagedump.css", new File(reportFolder.getAbsolutePath() + File.separator + "galen-pagedump.css"));
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:exportAllScreenshots
    protected File createObjectsFolder(File reportFolder) {
        File objectsFolder = new File(reportFolder.getAbsolutePath() + File.separator + "objects");
        objectsFolder.mkdirs();
        return objectsFolder;
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:exportAllScreenshots
    protected void takeElementScreenshot(Browser browser, PageElement element, String pageElementName, File reportFolder, File objectsFolder) throws IOException {
        BufferedImage image = getCurrentViewportScreenshot(browser, reportFolder);

        int[] area = element.getArea().toIntArray();

        if (area[0] < image.getWidth() && area[1] < image.getHeight()) {
            int x = Math.max(area[0], 0);
            int y = Math.max(area[1], 0);

            int x2 = Math.min(area[0] + area[2], image.getWidth());
            int y2 = Math.min(area[1] + area[3], image.getHeight());

            int availableWidth = x2 - x;
            int availableHeight = y2 - y;

            try {
                if (availableWidth > 0 && availableHeight > 0) {
                    BufferedImage subImage = image.getSubimage(x, y, availableWidth, availableHeight);
                    Rainbow4J.saveImage(subImage, new File(objectsFolder.getAbsolutePath() + File.separator + pageElementName + ".png"));
                }
            }
            catch (IOException e) {
                LOG.error("Got error during saving image", e);
                throw e;
            }
        }
    }

    // Extracted from Galen Framework: galen-core -> api/GalenPageDump.java:exportAllScreenshots
    private BufferedImage getCurrentViewportScreenshot(Browser browser, File reportFolder) throws IOException {
        File screenshotOriginalFile = browser.getPage().getScreenshotFile();
        FileUtils.copyFile(screenshotOriginalFile, new File(reportFolder.getAbsolutePath() + File.separator + "page.png"));
        return Rainbow4J.loadImage(screenshotOriginalFile.getAbsolutePath());
    }

    // Use original private method from Galen Framework: galen-core -> api/GalenPageDump.java
    private void copyResource(String resourceName, File destFile) throws IOException {
        try {
            Method declaredMethod = GalenPageDump.class.getDeclaredMethod("copyResource", String.class, File.class);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(this, resourceName, destFile);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            LOG.error("Error calling Galen private copyResource", e);
        }
    }
}
