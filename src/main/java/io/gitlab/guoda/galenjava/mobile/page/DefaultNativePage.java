package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.selenium.SeleniumPage;
import org.openqa.selenium.WebDriver;

public class DefaultNativePage extends SeleniumPage implements NativePage {

    public DefaultNativePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Fakes title for Native page apps. Used by some original Galen Framework methods, where SeleniumPage getTitle is called.
     */
    public String getTitle() {
        return "Native App page";
    }
}
