package io.gitlab.guoda.galenjava.mobile.page;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.scrollable.AndroidNativeScrollable;

public class AndroidNativeScrollablePage extends AndroidNativePage implements NativeScrollablePage {
    private AndroidNativeScrollable scroller;

    public AndroidNativeScrollablePage(AndroidDriver driver) {
        super(driver);
        scroller = new AndroidNativeScrollable(driver);
    }

    @Override
    public void scrollToElement(MobileElement mobileElement) {
        scroller.scrollToElement(mobileElement);
    }

    @Override
    public void scrollToElement(MobileElement mobileElement, int maxScrolls) {
        scroller.scrollToElement(mobileElement, maxScrolls);
    }

    @Override
    public void setPageTopElement(MobileElement pageTopElement) {
        scroller.setPageTopElement(pageTopElement);
    }

    @Override
    public void setPageBottomElement(MobileElement pageBottomElement) {
        scroller.setPageBottomElement(pageBottomElement);
    }
}
