package io.gitlab.guoda.galenjava.mobile.testng;

import com.galenframework.reports.TestReport;
import com.galenframework.reports.nodes.TestReportNode;
import com.galenframework.support.GalenJavaTestBase;
import com.galenframework.support.LayoutValidationException;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class GalenFunctionalTestNgReportsListener implements ITestListener {
    private static final Logger LOG = LoggerFactory.getLogger(GalenFunctionalTestNgReportsListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        LOG.debug(result.getName() + ": test started for device " + DeviceUnderTest.get().getName());
        TestReport galenReport = getGalenReport(result);
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        LOG.debug(result.getName() + ": test succeeded for device " + DeviceUnderTest.get().getName());
        logNotLayoutRelatedSuccess(result);
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LOG.debug(result.getName() + ": test failed for device " + DeviceUnderTest.get().getName());

        Throwable error = result.getThrowable();

        //Layout validation exception errors are logged using default Galen reporter, log only everything else
        if (error.getClass() != LayoutValidationException.class) {
            logNotLayoutRelatedError(result, error);
            LOG.debug("Error is: " + error.getMessage());
        }
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        LOG.info(result.getName() + ": test skipped for device " + DeviceUnderTest.get().getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        LOG.info(result.getName() + ": test failed within success percentage");
    }

    @Override
    public void onStart(ITestContext context) {
        LOG.debug(context.getName() + ": starting tests");
    }

    @Override
    public void onFinish(ITestContext context) {
        LOG.debug(context.getName() + ": finished tests");
    }

    private <T extends GalenJavaTestBase> TestReport getGalenReport(ITestResult result) {
        T currentTest = (T)result.getInstance();
        return currentTest.getReport();
    }

    private void logNotLayoutRelatedError(ITestResult result, Throwable er) {
        TestReportNode galenReportSection = startFunctionalTestLogSection(result,"Not Layout related tests failed");
        TestReport galenReport = getGalenReport(result);
        galenReport.error(er.getMessage());
        galenReportSection.setStatus(TestReportNode.Status.ERROR);
        endFunctionalTestLogSection(result);
    }

    private void logNotLayoutRelatedSuccess(ITestResult result) {
        TestReportNode galenReportSection = startFunctionalTestLogSection(result,"Not Layout related tests passed");
        galenReportSection.setStatus(TestReportNode.Status.INFO);
        endFunctionalTestLogSection(result);
    }

    private TestReportNode startFunctionalTestLogSection(ITestResult result, String sectionName) {
        TestReport galenReport = getGalenReport(result);
        return galenReport.sectionStart(sectionName);
    }

    private void endFunctionalTestLogSection(ITestResult result) {
        TestReport galenReport = getGalenReport(result);
        galenReport.sectionEnd();
    }
}
