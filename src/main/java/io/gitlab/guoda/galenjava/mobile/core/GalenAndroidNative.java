package io.gitlab.guoda.galenjava.mobile.core;

import com.galenframework.api.Galen;
import com.galenframework.browser.Browser;
import com.galenframework.page.Page;
import com.galenframework.reports.LayoutReportListener;
import com.galenframework.reports.model.LayoutReport;
import com.galenframework.speclang2.pagespec.PageSpecReader;
import com.galenframework.speclang2.pagespec.SectionFilter;
import com.galenframework.specs.page.Locator;
import com.galenframework.specs.page.PageSpec;
import com.galenframework.validation.*;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativeScrollablePage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class GalenAndroidNative extends Galen {
    private final static Logger LOG = LoggerFactory.getLogger(GalenAndroidNative.class);

    // Same as original Galen Framework function. Duplicated, as private function checkLayoutForPage override is needed
    public static LayoutReport checkLayout(Browser browser, String specPath,
                                           SectionFilter sectionFilter,
                                           Properties properties, Map<String, Object> jsVariables,
                                           File screenshotFile, ValidationListener validationListener,
                                           Map<String, Locator> objects) throws IOException {
        PageSpecReader reader = new PageSpecReader();
        PageSpec pageSpec = reader.read(specPath, browser.getPage(), sectionFilter, properties, jsVariables, objects);
        return checkLayout(browser, pageSpec, sectionFilter, screenshotFile, validationListener);
    }

    // Same as original Galen Framework function. Duplicated, as private function checkLayoutForPage override is needed
    public static LayoutReport checkLayout(Browser browser, PageSpec pageSpec,
                                           SectionFilter sectionFilter,
                                           File screenshotFile,
                                           ValidationListener validationListener) throws IOException {

        Page page = browser.getPage();
        page.setScreenshot(screenshotFile);

        return checkLayoutForPage(page, browser, pageSpec, sectionFilter, validationListener);
    }

    // Override of private Galen Framework function. Removed initial screenshot as image caching is not needed for Android Native.
    // Note: this removal disables HeatMap functionality in report, elements for succeeded tests will not be displayed.
    private static LayoutReport checkLayoutForPage(Page page, Browser browser, PageSpec pageSpec,
                                                       SectionFilter sectionFilter,
                                                       ValidationListener validationListener) throws IOException {

        CombinedValidationListener listener = new CombinedValidationListener();
        listener.add(validationListener);

        LayoutReport layoutReport = new LayoutReport();
        layoutReport.setIncludedTags(sectionFilter.getIncludedTags());
        layoutReport.setExcludedTags(sectionFilter.getExcludedTags());

        registerHeatMapScreenshot(page, layoutReport);

        listener.add(new LayoutReportListener(layoutReport));

        SectionValidation sectionValidation = new SectionValidation(pageSpec.getSections(), new PageValidation(browser, page, pageSpec, listener, sectionFilter), listener);

        List<ValidationResult> results = sectionValidation.check();
        List<ValidationResult> allValidationErrorResults = new LinkedList<>();

        for (ValidationResult result : results) {
            if (result.getError() != null) {
                allValidationErrorResults.add(result);
            }
        }

        layoutReport.setValidationErrorResults(allValidationErrorResults);

        return layoutReport;
    }

    private static void registerHeatMapScreenshot(Page page, LayoutReport layoutReport) {
        if (page instanceof GalenAndroidNativeScrollablePage) {
            LOG.debug("Page type is GalenAndroidNativeScrollablePage, no viewport screenshot is taken for HeatMap display in report log");
            return;
        }

        try {
            File screenshot = page.getScreenshotFile();
            if (screenshot != null) {
                layoutReport.setScreenshot(layoutReport.registerFile("screenshot.png", screenshot));
            }
        }
        catch (Exception e) {
            LOG.error("Error during setting screenshot", e);
        }
    }
}
