package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Optional;

public class AndroidNativeGalenLocator extends DefaultGalenLocator {
    protected Optional<Locator> getGalenLocator(Field field) throws GalenLocatorException {
        if (field.isAnnotationPresent(GalenFindBy.class)) {
            return Optional.of(getGalenLocatorByGalenAnnotation(field));
        }

        if (field.isAnnotationPresent(AndroidFindBy.class)) {
            return getGalenLocatorByAndroidFindByAnnotation(field);
        }

        return Optional.empty();
    }

    private Optional<Locator> getGalenLocatorByAndroidFindByAnnotation(Field field) {
        Annotation annotation = field.getAnnotation(AndroidFindBy.class);
        AndroidFindBy locatorAnnotations = (AndroidFindBy) annotation;

        if(!locatorAnnotations.id().equals("")) {
            return Optional.of(Locator.id(locatorAnnotations.id()));
        }
        if (!locatorAnnotations.xpath().equals("")) {
            return Optional.of(Locator.xpath(locatorAnnotations.xpath()));
        }
        if (!locatorAnnotations.accessibility().equals("")) {
            String xPathLocator = convertAccessibilityLocatorToXPath(locatorAnnotations.accessibility());
            return Optional.of(Locator.xpath(xPathLocator));
        }

        //else no error thrown: situation where element is identified by Android locator and not used in Galen tests is normal
        return Optional.empty();
    }
}
