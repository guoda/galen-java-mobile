package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class DefaultGalenLocatorTest {
    AppiumDriver driver = mock(AppiumDriver.class);

    @Test
    @DisplayName("Initializes Galen element and CheckImage locators")
    void initializesGalenElementAndCheckImageLocators() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenFindBy(id = "view")
            private MobileElement viewField;

            @GalenCheckImage
            @GalenFindBy(id = "img")
            private MobileElement elementToCheckByImage;

            public DemoPageClass(AppiumDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();
        Map<String, Locator> galenCheckImageLocators = targetTestPageObject.getGalenCheckImageLocators();

        assertAll("Galen locators",
                () -> assertEquals(2, galenLocators.size(), "Size not as expected"),
                () -> assertTrue(galenLocators.containsKey("viewField"), "viewField not found"),
                () -> assertTrue(galenLocators.containsKey("elementToCheckByImage"), "elementToCheckByImage not found")
        );

        assertAll("Galen checkImage locators",
                () -> assertEquals(1, galenCheckImageLocators.size(), "Size not as expected"),
                () -> assertTrue(galenCheckImageLocators.containsKey("elementToCheckByImage"), "elementToCheckByImage not found")
        );
    }

    @Test
    @DisplayName("Does not initialize GalenImageLocator when no GalenLocator is set")
    void doesNotInitializeGalenImageLocatorWhenGalenLocatorNotSet() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenCheckImage
            private MobileElement elementToCheckByImage;

            public DemoPageClass(AppiumDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();
        Map<String, Locator> galenCheckImageLocators = targetTestPageObject.getGalenCheckImageLocators();

        assertEquals(0, galenLocators.size(), "Galen locators size is other than expected");
        assertEquals(0, galenCheckImageLocators.size(), "Galen image locators size is other than expected");
    }

    @Test
    @DisplayName("Initializes GalenImageLocator for Page Object class when fields have no GalenCheckImage annotations")
    void initializesGalenImageLocatorForPageObjectClassWhenFieldsHaveNoGalenCheckImageAnnotations() {
        @GalenCheckImage
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenFindBy(id = "view")
            private MobileElement viewField;

            public DemoPageClass(AppiumDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);
        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();
        Map<String, Locator> galenCheckImageLocators = targetTestPageObject.getGalenCheckImageLocators();

        assertEquals(1, galenLocators.size(), "Galen locators size is other than expected");
        assertEquals(1, galenCheckImageLocators.size(), "Galen image locators size is other than expected");

        assertTrue(galenLocators.keySet().contains("viewField"), "Galen locators keys are other than expected");
        assertTrue(galenCheckImageLocators.keySet().contains("viewport"), "Galen image locators keys are other than expected");
    }

    @Test
    @DisplayName("Does not add additional nativeViewport Galen image locator when at least single field has GalenCheckImage annotation")
    void doesNotAddNativeViewportGalenImageLocatorWhenFieldContainsGalenCheckImageAnnotation() {
        @GalenCheckImage
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenCheckImage
            @GalenFindBy(id = "view")
            private MobileElement viewField;

            public DemoPageClass(AppiumDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);
        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();
        Map<String, Locator> galenCheckImageLocators = targetTestPageObject.getGalenCheckImageLocators();

        assertEquals(1, galenLocators.size(), "Galen locators size is other than expected");
        assertEquals(1, galenCheckImageLocators.size(), "Galen image locators size is other than expected");

        assertTrue(galenLocators.keySet().contains("viewField"), "Galen locators keys are other than expected");
        assertTrue(galenCheckImageLocators.keySet().contains("viewField"), "Galen image locators does not contain viewField");
        assertFalse(galenCheckImageLocators.keySet().contains("viewport"), "Galen image locators contains Viewport");
        assertEquals(null, galenCheckImageLocators.get("viewport"), "Locator value for Viewport is other than expected");
    }

}