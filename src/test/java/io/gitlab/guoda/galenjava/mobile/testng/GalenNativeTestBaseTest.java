package io.gitlab.guoda.galenjava.mobile.testng;

import io.appium.java_client.android.AndroidDriver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

import static java.util.Arrays.asList;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;

public class GalenNativeTestBaseTest {
    private GalenNativeTestBase targetTest;

    private TestDevice dummyAndroidTestDevice = new AndroidTestDevice("device1", asList("anyTag1")).setAppiumDeviceName("BH9000000C");
//    private TestDevice dummyUnknownDriverTestDevice = new AndroidTestDevice("device2", asList("anyTag1")).setAppiumDeviceName("BH9000000C");
//    private TestDevice dummyImplUndefinedDriverTestDevice = new AndroidTestDevice("device3", asList("anyTag1")).setAppiumDeviceName("BH9000000C");

    @BeforeEach
    void initDummyTestBase() {
        targetTest = new DummyTestBaseImpl();
    }

    @DisplayName("Initializes Android driver when device identification and Android driver creation is implemented by user in TestBaseImpl")
    @Test
    void initializesAndroidDriverWhenDeviceDriverIsIdentifiedAsAndroid() {
        Object[] initArgs = new Object[]{dummyAndroidTestDevice};
        targetTest.startAllDeviceDrivers(new Object[][]{initArgs});
        targetTest.initDriver(initArgs);

        WebDriver driver = targetTest.getDriver();

        assertThat(driver, instanceOf(AndroidDriver.class));
    }

//    @DisplayName("Throws error when driver type is not defined and no default driver creation is implemented by user in TestBaseImpl")
//    @Test
//    void throwsErrorWhenNoDefaultDriverTypeIsNotImplementedByUser() {
//        Object[] initArgs = new Object[]{dummyUnknownDriverTestDevice};
//
//        assertThrows(RuntimeException.class, () -> {
//            targetTest.initDriver(initArgs);
//        });
//    }
//
//    @DisplayName("Throws error when driver creation is not implemented by user in TestBaseImpl")
//    @Test
//    void throwsErrorWhenDriverCreationIsNotImplementedByUser() {
//        Object[] initArgs = new Object[]{dummyImplUndefinedDriverTestDevice};
//
//        assertThrows(RuntimeException.class, () -> {
//            targetTest.initDriver(initArgs);
//        });
//    }
}
