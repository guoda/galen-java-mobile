package io.gitlab.guoda.galenjava.mobile.scrollable;

import io.appium.java_client.MobileElement;

public interface NativeScrollable {
    void scrollToElement(MobileElement mobileElement);
    void scrollToElement(MobileElement mobileElement, int maxScrolls);
    void setPageTopElement(MobileElement pageTopElement);
    void setPageBottomElement(MobileElement pageBottomElement);
}
