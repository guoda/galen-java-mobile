package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DefaultGalenLocator implements GalenLocator {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultGalenLocator.class);

    private Map<String, Locator> galenLocators = new HashMap<>();
    private Map<String, Locator> galenCheckImageLocators = new HashMap<>();

    /**
     * Initializes Galen Locators and Galen Check Image Locators for a test page.
     * @param testPage
     */
    public void initGalenLocators(GalenNativeTestPageBase testPage) {
        Class<?> obj = testPage.getClass();
        getFieldGalenLocators(obj);

        /*
            If Page Object class itself has GalenCheckImage annotation, and no images of fields are required,
            add PO itself to galenCheckImageLocators list to minimize amount of screenshots which will be taken.
         */
        if (getGalenCheckImageLocators().isEmpty()) {
            getClassGalenLocators(obj);
        }
    }

    /*
        Adds viewport to checkImage locators list.
        Sets null as Galen locator: Galen Framework searches for special object by object name when locator is null.
     */
    private void getClassGalenLocators(Class<?> obj) {
        if (obj.isAnnotationPresent(GalenCheckImage.class)) {
            galenCheckImageLocators.put("viewport", null);
        }
    }

    private void getFieldGalenLocators(Class<?> obj) {
        for (Field field : obj.getDeclaredFields()) {
            String fieldName = field.getName();
            try {
                Optional<Locator> galenLocator = getGalenLocator(field);
                galenLocator.ifPresent(locator -> {
                    galenLocators.put(fieldName, locator);
                    if (isGalenCheckImageLocator(field)) {
                        galenCheckImageLocators.put(fieldName, locator);
                    }
                });
            } catch (GalenLocatorException e) {
                String errorMessage = "Cannot initialize Galen locator for field " + fieldName;
                throw new RuntimeException(errorMessage, e);
            }
        }
    }

    public Map<String, Locator> getGalenLocators() {
        return galenLocators;
    }

    public Map<String, Locator> getGalenCheckImageLocators() {
        return galenCheckImageLocators;
    }

    protected Optional<Locator> getGalenLocator(Field field) throws GalenLocatorException {
        if (field.isAnnotationPresent(GalenFindBy.class)) {
            return Optional.of(getGalenLocatorByGalenAnnotation(field));
        }

        return Optional.empty();
    }

    protected Locator getGalenLocatorByGalenAnnotation(Field field) throws GalenLocatorException {
        Annotation annotation = field.getAnnotation(GalenFindBy.class);
        GalenFindBy locatorAnnotations = (GalenFindBy) annotation;
        if(!locatorAnnotations.id().equals("")) {
            return Locator.id(locatorAnnotations.id());
        }
        if (!locatorAnnotations.css().equals("")) {
            return Locator.css(locatorAnnotations.css());
        }
        if (!locatorAnnotations.xpath().equals("")){
            return Locator.xpath(locatorAnnotations.xpath());
        }
        if (!locatorAnnotations.accessibility().equals("")) {
            String xPathLocator = convertAccessibilityLocatorToXPath(locatorAnnotations.accessibility());
            return Locator.xpath(xPathLocator);
        }
        throw new GalenLocatorException("Field " + field.getName() + " error: All Galen locators are empty");
    }

    //Assume accessibility locators are unique. Returns first found element with matching content-desc for Android
    protected String convertAccessibilityLocatorToXPath(String accessibilityLocator) {
        return "(//*[@content-desc='" + accessibilityLocator + "'])[1]";
    }

    protected boolean isGalenCheckImageLocator(Field field) {
        return field.isAnnotationPresent(GalenCheckImage.class);
    }

    public Map<String, Locator> getGalenDumpImageLocators() {
        if (galenCheckImageLocators.isEmpty()) {
            return galenLocators;
        }
        return galenCheckImageLocators;
    }

}
