package io.gitlab.guoda.galenjava.mobile.testpage;

import com.galenframework.reports.TestReport;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.gitlab.guoda.galenjava.mobile.locator.AndroidNativeGalenLocator;
import io.gitlab.guoda.galenjava.mobile.locator.DefaultGalenLocator;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenDefaultNativeScrollablePage;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDevice;
import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;

import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

class GalenNativeScrollableTestPageBaseTest {
    class DummyScrollableNativeTestPage extends GalenNativeScrollableTestPageBase {
        public DummyScrollableNativeTestPage(AppiumDriver driver) {
            super(driver);
        }
    }

    @BeforeAll
    static void initMockTest() {
        TestDevice device = new AndroidTestDevice("DummyTestDevice", new Dimension(750, 1000), asList("anyTag"));
        DeviceUnderTest.set(device);
    }

    @AfterAll
    static void teardownMockTest() {
        DeviceUnderTest.unset();
    }

    @Test
    @DisplayName("Returns GalenDefaultNativeScrollablePage when generic Appium driver is provided")
    void returnsGalenDefaultNativeScrollablePageForGenericAppiumDriver() {
        AppiumDriver driver = mock(AppiumDriver.class);

        GalenNativeScrollableTestPageBase targetPage = new DummyScrollableNativeTestPage(driver);

        assertEquals(GalenDefaultNativeScrollablePage.class, targetPage.getPage().getClass());
    }

    @Test
    @DisplayName("Returns GalenAndroidNativeScrollablePage when Android driver is provided")
    void returnsGalenAndroidNativeScrollablePageForAndroidDriver() {
        AndroidDriver driver = mock(AndroidDriver.class);

        GalenNativeScrollableTestPageBase targetPage = new DummyScrollableNativeTestPage(driver);

        assertEquals(GalenAndroidNativeScrollablePage.class, targetPage.getPage().getClass());
    }

    @Test
    @DisplayName("Returns GalenDefaultNativeScrollablePage when iOS driver is provided")
    void returnsGalenDefaultNativeScrollablePageForIosDriver() {
        IOSDriver driver = mock(IOSDriver.class);

        GalenNativeScrollableTestPageBase targetPage = new DummyScrollableNativeTestPage(driver);

        assertEquals(GalenDefaultNativeScrollablePage.class, targetPage.getPage().getClass());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Allows checking layout for pages of GalenNativePage kind for scrollable page test base")
    void allowsCheckingLayoutForGalenNativePageKindPagesForScrollableBase() throws IOException {
        AndroidDriver driver = mock(AndroidDriver.class);
        List<String> dummyTagList = mock(List.class);
        TestReport dummyReport = mock(TestReport.class);

        GalenNativeScrollableTestPageBase targetPage = new DummyScrollableNativeTestPage(driver);
        GalenNativeScrollableTestPageBase targetPageSpy = spy(targetPage);
        doNothing().when(targetPageSpy).checkCurrentPageLayout(dummyTagList, dummyReport);

        targetPageSpy.checkLayout(dummyTagList, dummyReport);

        verify(targetPageSpy, times(1)).checkCurrentPageLayout(dummyTagList, dummyReport);
    }
}