package io.gitlab.guoda.galenjava.mobile.scrollable;

import com.galenframework.page.PageElement;
import com.galenframework.specs.page.Locator;

public interface GalenNativeScrollable extends NativeScrollable {
    PageElement scrollToElement(Locator locator, int maxScrolls);
}
