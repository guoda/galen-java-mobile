package io.gitlab.guoda.galenjava.mobile.core;

import com.galenframework.api.Galen;
import com.galenframework.browser.Browser;
import com.galenframework.reports.TestReport;
import com.galenframework.reports.model.LayoutReport;
import com.galenframework.speclang2.pagespec.SectionFilter;
import com.galenframework.specs.page.Locator;
import com.galenframework.support.LayoutValidationException;
import io.gitlab.guoda.galenjava.mobile.browser.AndroidBrowser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class GalenLayoutNativeAsserter implements GalenLayoutAsserter {
    private static final Logger LOG = LoggerFactory.getLogger(GalenLayoutNativeAsserter.class);

    private Browser browser;
    private TestReport galenReport;

    // Extracted from galen-java-support: GalenJavaTestBase -> checkLayout
    public GalenLayoutNativeAsserter(Browser browser, TestReport report) {
        this.browser = browser;
        this.galenReport = report;
    }

    public void checkLayout(String specPath, List<String> includedTags, Map<String, Locator> galenLocators) throws IOException {
        String title = "Check layout " + specPath;
        SectionFilter sectionFilter = new SectionFilter(includedTags, Collections.emptyList());

        LayoutReport layoutReport = getLayoutReport(specPath, sectionFilter, galenLocators);

        layoutReport(title, layoutReport, specPath, sectionFilter);
    }

    protected LayoutReport getLayoutReport(String specPath, SectionFilter sectionFilter, Map<String, Locator> galenLocators) throws IOException {
        if (browser instanceof AndroidBrowser) {
            return GalenAndroidNative.checkLayout(browser, specPath, sectionFilter, new Properties(), (Map)null, null, null, galenLocators);
        }

        return Galen.checkLayout(browser, specPath, sectionFilter, new Properties(), (Map)null, null, null, galenLocators);
    }

    private void layoutReport(String title, LayoutReport layoutReport, String specPath, SectionFilter sectionFilter) {
        galenReport.layout(layoutReport, title);
        if (layoutReport.errors() > 0) {
            throw new LayoutValidationException(specPath, layoutReport, sectionFilter);
        }
    }
}
