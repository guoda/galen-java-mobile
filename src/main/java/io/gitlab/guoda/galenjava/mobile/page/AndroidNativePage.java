package io.gitlab.guoda.galenjava.mobile.page;

import io.appium.java_client.android.AndroidDriver;

public class AndroidNativePage extends DefaultNativePage {
    public AndroidNativePage(AndroidDriver driver){
        super(driver);
    }
}
