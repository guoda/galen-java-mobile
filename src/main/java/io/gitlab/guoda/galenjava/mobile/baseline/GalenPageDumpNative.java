package io.gitlab.guoda.galenjava.mobile.baseline;

import com.galenframework.browser.Browser;
import com.galenframework.browser.SeleniumBrowser;
import com.galenframework.specs.page.Locator;
import com.galenframework.specs.page.PageSpec;

import java.io.IOException;
import java.util.Map;

public interface GalenPageDumpNative {
    void dumpPageImages(String pageBaselinePath, Map<String, Locator> galenDumpImageLocators);
    void dumpPage(Browser browser, PageSpec pageSpec, String reportFolderPath) throws IOException;
}
