package io.gitlab.guoda.galenjava.mobile.testpage;

import com.galenframework.reports.TestReport;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.gitlab.guoda.galenjava.mobile.page.DefaultNativePage;
import io.gitlab.guoda.galenjava.mobile.page.GalenAndroidNativePage;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDevice;
import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GalenNativeTestPageBaseTest {

    class DummyNonScrollableNativeTestPage extends GalenNativeTestPageBase {
        public DummyNonScrollableNativeTestPage(AppiumDriver driver) {
            super(driver);
        }
    }

    @BeforeAll
    static void initMockTest() {
        TestDevice device = new AndroidTestDevice("DummyTestDevice", new Dimension(750, 1000), asList("anyTag"));
        DeviceUnderTest.set(device);
    }

    @AfterAll
    static void teardownMockTest() {
        DeviceUnderTest.unset();
    }

    @Test
    @DisplayName("Returns DefaultNativePage when generic Appium driver is provided")
    void returnsDefaultNativePageForGenericAppiumDriver() {
        AppiumDriver driver = mock(AppiumDriver.class);

        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);

        assertEquals(DefaultNativePage.class, targetPage.getPage().getClass());
    }

    @Test
    @DisplayName("Returns GalenAndroidNativePage when Android driver is provided")
    void returnsGalenAndroidNativePageForAndroidDriver() {
        AndroidDriver driver = mock(AndroidDriver.class);

        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);

        assertEquals(GalenAndroidNativePage.class, targetPage.getPage().getClass());
    }

    @Test
    @DisplayName("Returns DefaultNativePage when Appium driver having no DefaultNativePage implementation is provided")
    void returnsDefaultNativePageForOtherAppiumDriver() {
        IOSDriver driver = mock(IOSDriver.class);

        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);

        assertEquals(DefaultNativePage.class, targetPage.getPage().getClass());
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Does not allow checking layout for pages of other kind than GalenNativePage")
    void doesNotAllowCheckingLayoutForNonGalenNativePageKindPages() {
        IOSDriver driver = mock(IOSDriver.class);
        List<String> dummyTagList = mock(List.class);
        TestReport dummyReport = mock(TestReport.class);

        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);

        assertThrows(RuntimeException.class, () -> {
            targetPage.checkLayout(dummyTagList, dummyReport);
        });
    }

    @Test
    @SuppressWarnings("unchecked")
    @DisplayName("Allows checking layout for pages of GalenNativePage kind for non-scrollable page test base")
    void allowsCheckingLayoutForGalenNativePageKindPagesForNonScrollableBase() throws IOException {
        AndroidDriver driver = mock(AndroidDriver.class);
        List<String> dummyTagList = mock(List.class);
        TestReport dummyReport = mock(TestReport.class);

        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);
        GalenNativeTestPageBase targetPageSpy = spy(targetPage);
        doNothing().when(targetPageSpy).checkCurrentPageLayout(dummyTagList, dummyReport);

        targetPageSpy.checkLayout(dummyTagList, dummyReport);

        verify(targetPageSpy, times(1)).checkCurrentPageLayout(dummyTagList, dummyReport);
    }

    @Test
    @DisplayName("Uses image baseline root from configuration file when provided")
    void usesImageBaselineFromConfigurationFileWhenProvided() {
        AndroidDriver driver = mock(AndroidDriver.class);
        GalenNativeTestPageBase targetPage = new DummyNonScrollableNativeTestPage(driver);
        targetPage.setPageGalenSpecPath("spec/dummyPage.spec");

        targetPage.setPageScreenshotsBaselinePath(DeviceUnderTest.get().toFilePath());

        String basePath = new File("").getAbsolutePath();
        String expectedPath =
                basePath + File.separator +
                "src/test/resources/baseline" + File.separator +
                DeviceUnderTest.get().toFilePath() + File.separator +
                "dummyPage";

        assertEquals(expectedPath, targetPage.getPageScreenshotsBaselinePath());
    }
}