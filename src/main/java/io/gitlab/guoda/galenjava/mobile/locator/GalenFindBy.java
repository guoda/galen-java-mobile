package io.gitlab.guoda.galenjava.mobile.locator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Used with Appium Page Object Factory to set GalenFindBy for MobileElement.
 * When annotation is set, Galen layout tests will use locator defined in annotation.
 * When no annotation is set, Galen layout tests will try to use @AndroidFindBy annotation.
 * No need to set when GalenFindBy is same as @AndroidFindBy.
 * When both annotations are set, GalenFindBy is used to locate elements in Galen visual tests.
 */
@Retention(RUNTIME) @Target({FIELD})
public @interface GalenFindBy {
    String id() default "";
    String xpath() default "";
    String css() default "";
    String accessibility() default "";
}
