package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;
import io.gitlab.guoda.galenjava.mobile.testpage.GalenNativeTestPageBase;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

class AndroidNativeGalenLocatorTest {
    AndroidDriver driver = mock(AndroidDriver.class);

    @BeforeAll
    static void initMockTestDevice() {
        TestDevice device = mock(TestDevice.class, RETURNS_DEEP_STUBS);
        DeviceUnderTest.set(device);
    }

    @AfterAll
    static void teardownMockTestDevice() {
        DeviceUnderTest.unset();
    }

    @Test
    @DisplayName("Gets Galen locator by GalenFindBy ignoring AndroidFindBy when both annotations are available")
    void getGalenLocatorByGalenFindByIgnoringAndroidFindBy() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenFindBy(id = "view")
            @AndroidFindBy(id = "something Android specific")
            private MobileElement viewField;

            public DemoPageClass(WebDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();

        assertEquals(1, galenLocators.size(), "Galen locators amount not as expected");
        assertTrue(galenLocators.containsKey("viewField"));
        assertEquals("view", galenLocators.get("viewField").getLocatorValue(), "Field value is other than expected");
        assertEquals("id", galenLocators.get("viewField").getLocatorType(), "Field type is other than expected");
    }

    @Test
    @DisplayName("Gets Galen locator by mapped to Galen locator AndroidFindBy when no GalenFindBy annotation is available")
    void getGalenLocatorByMappedAndroidFindByWhenNoGalenFindByAvailable() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @AndroidFindBy(id = "view")
            private MobileElement viewField;

            @AndroidFindBy(xpath = "//add")
            private MobileElement addField;

            @AndroidFindBy(accessibility = "My Button Text")
            private MobileElement button;

            public DemoPageClass(WebDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();

        assertEquals(3, galenLocators.size(), "Galen locators amount not as expected");

        assertAll("View field",
                () -> assertTrue(galenLocators.containsKey("viewField"), "Field does not exist"),
                () -> assertEquals("view", galenLocators.get("viewField").getLocatorValue(), "Field value is other than expected"),
                () -> assertEquals("id", galenLocators.get("viewField").getLocatorType(), "Field type is other than expected")
        );

        assertAll("Add field",
                () -> assertTrue(galenLocators.containsKey("addField"), "Field does not exist"),
                () -> assertEquals("//add", galenLocators.get("addField").getLocatorValue(), "Field value is other than expected"),
                () -> assertEquals("xpath", galenLocators.get("addField").getLocatorType(), "Field type is other than expected")
        );

        assertAll("Button field",
                () -> assertTrue(galenLocators.containsKey("button"), "Field does not exist"),
                () -> assertEquals("(//*[@content-desc='My Button Text'])[1]", galenLocators.get("button").getLocatorValue(), "Field value is other than expected"),
                () -> assertEquals("xpath", galenLocators.get("button").getLocatorType(), "Field type is other than expected")
        );
    }

    @Test
    @DisplayName("Gets no Galen locator when not mapped AndroidFindBy is used")
    void getNoGalenLocatorWhenUnmappedAndroidFindByIsUsed() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @AndroidFindBy(className = "myClass")
            private MobileElement viewField;

            public DemoPageClass(WebDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();

        assertEquals(0, galenLocators.size(), "Galen locators amount not as expected");
    }

    @Test
    @DisplayName("Ignores AndroidFindBy when no Galen Annotation value is defined")
    void throwsErrorWhenNoGalenAnnotationValueIsDefinedAndIgnoresAndroidFindBy() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenFindBy
            @AndroidFindBy(id = "view")
            private MobileElement viewField;

            public DemoPageClass(WebDriver driver){
                super(driver);
            }
        }

        assertThrows(RuntimeException.class, () -> {
            new DemoPageClass(driver);
        });
    }

    @Test
    @DisplayName("Initializes Galen Image Locators using AndroidFindBy when no Galen Locators are available")
    void initializesGalenImageLocatorsUsingAndroidFindByWhenNoGalenLocatorsAvailable() {
        class DemoPageClass extends GalenNativeTestPageBase {
            @GalenCheckImage
            @AndroidFindBy(id = "view")
            private MobileElement viewField;

            public DemoPageClass(WebDriver driver){
                super(driver);
            }
        }

        DemoPageClass targetTestPageObject = new DemoPageClass(driver);

        Map<String, Locator> galenLocators = targetTestPageObject.getGalenLocators();
        Map<String, Locator> galenCheckImageLocators = targetTestPageObject.getGalenCheckImageLocators();

        assertEquals(1, galenLocators.size(), "Galen locators size is other than expected");
        assertEquals(1, galenCheckImageLocators.size(), "Galen image locators size is other than expected");
        assertEquals("view", galenCheckImageLocators.get("viewField").getLocatorValue(), "Image locator value is other than expacted");
        assertEquals("id", galenCheckImageLocators.get("viewField").getLocatorType(), "Image locator type is other than expacted");
    }
}