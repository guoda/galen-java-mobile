package io.gitlab.guoda.galenjava.mobile.testng;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.Dimension;

import java.net.URL;
import java.util.List;

public class AndroidTestDevice extends TestDevice {
    private static final String deviceType = "android";
    private String appiumDeviceName;
    private String appiumServerBaseUrl;
    private int appiumSystemPort = 8200;
    private String udid;
    private AndroidTestDeviceConfiguration configuration;

    public AndroidTestDevice(String name) {
        super(name);
    }

    public AndroidTestDevice(String name, List<String> tags) {
        super(name, tags);
    }

    public AndroidTestDevice(String name, Dimension screenSize, List<String> tags) {
        super(name, screenSize, tags);
    }

    /**
     * Sets device identificator to be used by Appium when initializing driver for this device.
     * This does NOT set Appium mobile capability itself, is used for storing device properties configuration.
     * @param name Appium device or emulator name, used by mobile capability "deviceName"
     * @return Android test device instance with Appium device name
     */
    public AndroidTestDevice setAppiumDeviceName(String name) {
        this.appiumDeviceName = name;
        return this;
    }

    public String getAppiumDeviceName() {
        return appiumDeviceName;
    }

    @Override
    public void loadConfiguration() {
        this.configuration = new AndroidTestDeviceConfiguration(this);
    }

    @Override
    public AndroidTestDeviceConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public AndroidDriver getDriver() {
        return (AndroidDriver) super.getDriver();
    }

    public URL getAppiumServerBaseUrl() {
        try {
            return new URL(appiumServerBaseUrl);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets base URL to be used by Appium for executing tests on this device.
     * This does NOT set Appium mobile capability itself, is used for storing device properties configuration.
     * @param appiumServerBaseUrl Appium server base URL, which will be used for communication with device. E.g.: "http://127.0.0.1:4723/wd/hub"
     * @return Android test device instance with value which should be used as Appium base server URL when creating Android driver
     */
    public AndroidTestDevice setAppiumServerBaseUrl(String appiumServerBaseUrl) {
        this.appiumServerBaseUrl = appiumServerBaseUrl;
        return this;
    }

    public int getAppiumSystemPort() {
        return appiumSystemPort;
    }

    /**
     * Sets Appium "systemPort" capability value for device. Should be unique for each device when running tests in parallel.
     * This does NOT set Appium mobile capability itself, is used for storing device properties configuration.
     * @param appiumSystemPort systemPort used to connect to appium-uiautomator2-server. Defaults to 8200 if unset.
     * @return Android test device instance with value which should be used for Appium "systemPort" capability
     */
    public AndroidTestDevice setAppiumSystemPort(int appiumSystemPort) {
        this.appiumSystemPort = appiumSystemPort;
        return this;
    }

    public String getUdid() {
        return udid;
    }

    /**
     * Sets Appium "udid" capability value for device. Must be set when more than single device is used for running tests.
     * This does NOT set Appium mobile capability itself, is used for storing device properties configuration.
     * @param udid
     * @return Android test device instance with value which should be used for Appium "udid" capability
     */
    public AndroidTestDevice setUdid(String udid) {
        this.udid = udid;
        return this;
    }
}
