package io.gitlab.guoda.galenjava.mobile.utils;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public abstract class GalenJavaMobileConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(GalenJavaMobileConfiguration.class);

    private static final String CONFIGURATION_FILE = "galenjavaMobile.properties";
    private static final Configurations config = new Configurations();
    protected static final PropertiesConfiguration properties = loadProperties();

    protected static final String SYSTEM_PREFIX = "galenjava.mobile.";

    private static PropertiesConfiguration loadProperties() {
        PropertiesConfiguration properties = new PropertiesConfiguration();
        try {
            properties = config.properties(new File(CONFIGURATION_FILE));
        } catch (ConfigurationException e) {
            LOG.debug("Could not load configuration properties from " + CONFIGURATION_FILE + " file, using default values");
        }
        return properties;
    }

}
