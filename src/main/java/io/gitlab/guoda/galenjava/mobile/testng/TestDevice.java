package io.gitlab.guoda.galenjava.mobile.testng;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;

import java.util.List;

import static java.util.Arrays.asList;

public abstract class TestDevice {
    private final String name;
    private Dimension screenSize;
    private final List<String> tags;
    private WebDriver driver;

    public TestDevice(String name) {
        this.name = name;
        this.tags = asList(name);
    }

    public TestDevice(String name, List<String> tags) {
        this.name = name;
        this.tags = tags;
    }

    public TestDevice(String name, Dimension screenSize, List<String> tags) {
        this(name, tags);
        this.screenSize = screenSize;
    }

    public String getName() {
        return name;
    }

    /**
     *  Gets tags associated with device which are used in Galen Framework spec file to identify device specific requirements.
     * @return list of tags
     */
    public List<String> getTags() {
        return tags;
    }

    public void setScreenSize(Dimension screenSize) {
        this.screenSize = screenSize;
    }

    public Dimension getScreenSize() {
        return screenSize;
    }

    public int getScreenHeight() {
        return screenSize.getHeight();
    }

    public int getScreenWidth() {
        return screenSize.getWidth();
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }

    public String toFilePath() {
        return name.replaceAll(" ", "_");
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public abstract TestDeviceConfiguration getConfiguration();
    public abstract void loadConfiguration();
    public abstract String getDeviceType();
}
