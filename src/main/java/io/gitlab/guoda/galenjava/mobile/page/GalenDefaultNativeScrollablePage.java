package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.PageElement;
import com.galenframework.specs.page.Locator;
import io.appium.java_client.MobileElement;
import io.gitlab.guoda.galenjava.mobile.scrollable.GalenNativeScrollable;
import org.openqa.selenium.WebDriver;

public class GalenDefaultNativeScrollablePage extends DefaultNativePage implements NativeScrollablePage, GalenNativeScrollable, GalenNativePage {
    public GalenDefaultNativeScrollablePage(WebDriver driver) {
        super(driver);
    }

    /**
     * Default scroll tries to reach element using standard Selenium 'getObject' method from Galen Framework.
     * Override as needed if this is not enough for custom native app driver to reach element.
     * @param locator Galen locator of element to get
     * @param maxScrolls max amount of scrolls to use before stopping element search. Is ignored for default method.
     * @return
     */
    @Override
    public PageElement scrollToElement(Locator locator, int maxScrolls) {
        return getObject(locator);
    }

    /**
     * Does nothing for DefaultNativeScrollablePage.
     * Override as needed to scroll to MobileElement for custom native app driver.
     * @param mobileElement
     */
    @Override
    public void scrollToElement(MobileElement mobileElement) {
        //do nothing for default page
    }

    /**
     * Does nothing for DefaultNativeScrollablePage.
     * Override as needed to scroll to MobileElement using predefined max amount of scrolls for custom native app driver.
     * @param mobileElement
     * @param maxScrolls
     */
    @Override
    public void scrollToElement(MobileElement mobileElement, int maxScrolls) {
        //do nothing for default page
    }

    /**
     * Does nothing for DefaultNativeScrollablePage.
     * Override as needed to set page top element for custom native app driver.
     * @param pageTopElement
     */
    @Override
    public void setPageTopElement(MobileElement pageTopElement) {
        //do nothing for default page
    }

    /**
     * Does nothing for DefaultNativeScrollablePage.
     * Override as needed to set page bottom element for custom native app driver.
     * @param pageBottomElement
     */
    @Override
    public void setPageBottomElement(MobileElement pageBottomElement) {
        //do nothing for default page
    }
}
