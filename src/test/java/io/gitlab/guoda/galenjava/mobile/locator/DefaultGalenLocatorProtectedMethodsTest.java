package io.gitlab.guoda.galenjava.mobile.locator;

import com.galenframework.specs.page.Locator;
import io.appium.java_client.MobileElement;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class DefaultGalenLocatorProtectedMethodsTest extends DefaultGalenLocator {
    @Test
    @DisplayName("Gets Galen locator by Galen ID annotation")
    void getGalenLocatorByGalenIdAnnotation() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(id = "view")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("view", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("id", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Gets Galen locator by Galen CSS annotation")
    void getGalenLocatorByGalenCssAnnotation() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(css = "[myAttr='view']")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("[myAttr='view']", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("css", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Gets Galen locator by Galen XPath annotation")
    void getGalenLocatorByGalenXPathAnnotation() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(xpath = "//view")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("//view", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("xpath", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Gets Galen locator by Galen Accessibility annotation as XPath locator")
    void getGalenLocatorByGalenAccessibilityAnnotationAsXPathLocator() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(accessibility = "View Button Text")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("(//*[@content-desc='View Button Text'])[1]", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("xpath", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Uses Galen ID annotation as preferred when it is available and ignores all other annotations")
    void usesGalenIdAnnotationAsPreferredAnnotation() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(xpath = "//view", accessibility = "View Button", id = "view", css = "[myAttr='view']")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("view", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("id", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Uses Galen CSS annotation as preferred when no ID annotation is available")
    void usesGalenCssAnnotationAsPreferredAnnotationWhenNoIdAvailable() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(xpath = "//view", accessibility = "View Button", css = "[myAttr='view']")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("[myAttr='view']", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("css", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Uses Galen XPath annotation as preferred when no ID or CSS annotations are available")
    void usesGalenXPathAnnotationAsPreferredAnnotationWhenNoIdOrCssAvailable() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            @GalenFindBy(xpath = "//view", accessibility = "View Button", css = "", id = "")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);
        Locator galenLocator = optionalGalenLocator.get();

        assertEquals("//view", galenLocator.getLocatorValue(), "Galen locator value is other than expected");
        assertEquals("xpath", galenLocator.getLocatorType(), "Galen locator type is other than expected");
    }

    @Test
    @DisplayName("Throws error when all Galen Annotations are empty")
    void throwsErrorWhenAllGalenAnnotationAreEmpty() throws NoSuchFieldException {
        class DemoPageClass {
            @GalenFindBy(xpath = "", css = "", id = "", accessibility = "")
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        assertThrows(GalenLocatorException.class, () -> {
            getGalenLocator(field);
        });
    }

    @Test
    @DisplayName("Throws error when no Galen Annotation value is defined")
    void throwsErrorWhenNoGalenAnnotationValueIsDefined() throws NoSuchFieldException {
        class DemoPageClass {
            @GalenFindBy
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        assertThrows(GalenLocatorException.class, () -> {
            getGalenLocator(field);
        });
    }

    @Test
    @DisplayName("Returns empty locator when no Galen Annotations are available")
    void returnsEmptyLocatorWhenNoGalenAnnotationsAreAvailable() throws NoSuchFieldException, GalenLocatorException {
        class DemoPageClass {
            public MobileElement viewField;
        }

        Field field = DemoPageClass.class.getField("viewField");

        Optional<Locator> optionalGalenLocator = getGalenLocator(field);

        assertEquals(Optional.empty(), optionalGalenLocator);
    }

}
