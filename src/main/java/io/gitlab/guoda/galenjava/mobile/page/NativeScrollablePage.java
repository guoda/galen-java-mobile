package io.gitlab.guoda.galenjava.mobile.page;

import io.gitlab.guoda.galenjava.mobile.scrollable.NativeScrollable;

public interface NativeScrollablePage extends NativePage, NativeScrollable {
}
