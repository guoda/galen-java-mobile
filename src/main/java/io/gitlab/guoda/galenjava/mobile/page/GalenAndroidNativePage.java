package io.gitlab.guoda.galenjava.mobile.page;

import com.galenframework.page.PageElement;
import com.galenframework.rainbow4j.Rainbow4J;
import io.appium.java_client.android.AndroidDriver;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDeviceConfiguration;
import io.gitlab.guoda.galenjava.mobile.testng.TestDevice;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.openqa.selenium.OutputType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class GalenAndroidNativePage extends AndroidNativePage implements GalenNativePage {
    private static final Logger LOG = LoggerFactory.getLogger(GalenAndroidNativePage.class);
    private AndroidDriver driver;
    private TestDevice currentDevice;

    public GalenAndroidNativePage(AndroidDriver driver) {
        super(driver);
        this.driver = driver;
        currentDevice = DeviceUnderTest.get();
    }

    /**
     * Creates screenshot file with masked Android status bar ('charging', 'wifi' etc. indicators are hidden).
     * @return
     */
    public File getScreenshotFile() {
        File currentViewportScreenshotFile, modifiedScreenshotFile;

        LOG.debug("Getting screenshot for Android device...");

        currentViewportScreenshotFile = driver.getScreenshotAs(OutputType.FILE);
        modifiedScreenshotFile = maskAndroidStatusBar(currentViewportScreenshotFile);

        // some native Galen methods rely on cached images / screenshot files, update to current screenshot
        setScreenshot(modifiedScreenshotFile);

        return modifiedScreenshotFile;
    }

    /*
        Used by SpecValidationImage; removed image caching for Android Native
    */
    @Override
    public BufferedImage getScreenshotImage() {
        BufferedImage currentViewportImage;

        try {
            currentViewportImage = Rainbow4J.loadImage(getScreenshotFile().getAbsolutePath());
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load screenshot image for page", e);
        }

        return currentViewportImage;
    }

    /**
     * Returns 'special' object as defined by Galen Framework. Added additional 'nativeViewport' type of special object for native mobile apps.
     * @param objectName special object type name
     * @return page element for required special object type
     */
    @Override
    public PageElement getSpecialObject(String objectName) {
        PageElement pageElement = null;

        switch (objectName) {
            case "viewport":
                pageElement = new AndroidNativeViewportElement(driver);
                break;
            default:
                pageElement = super.getSpecialObject(objectName);
        }

        return pageElement;
    }

    /*
        Masks upper status bar (charging, WiFi etc. icons) in screenshot
    */
    private File maskAndroidStatusBar(File image){
        BufferedImage img;
        File out;
        AndroidTestDeviceConfiguration CONFIG = (AndroidTestDeviceConfiguration) DeviceUnderTest.get().getConfiguration();

        LOG.debug("Masking status bar for Android device " + currentDevice.getName());
        LOG.debug("Device screen is: " + currentDevice.getScreenWidth() + " x " + currentDevice.getScreenHeight());
        LOG.debug("Computed status bar height is: " + CONFIG.ANDROID_UPPER_STATUS_BAR_HEIGHT);

        try {
            out = File.createTempFile("modified", "png");
            out.deleteOnExit();
            img = ImageIO.read(image);

            Graphics2D g = img.createGraphics();
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, currentDevice.getScreenWidth(), CONFIG.ANDROID_UPPER_STATUS_BAR_HEIGHT);
            g.dispose();

            ImageIO.write(img,"png", out);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't mask Android status bar for screenshot", e);
        }
        return out;
    }
}
