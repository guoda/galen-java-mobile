package io.gitlab.guoda.galenjava.mobile.page;

public interface NativePage {
    String getTitle();
}
