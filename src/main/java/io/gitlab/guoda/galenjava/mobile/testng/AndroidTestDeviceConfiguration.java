package io.gitlab.guoda.galenjava.mobile.testng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.NoSuchElementException;

public class AndroidTestDeviceConfiguration extends TestDeviceConfiguration {
    private static final Logger LOG = LoggerFactory.getLogger(AndroidTestDeviceConfiguration.class);

    private static final String ANDROID_PREFIX = SYSTEM_PREFIX + "android.";

    private String ANDROID_SCROLL_MAX_AMOUNT_KEY;
    static final int ANDROID_DEFAULT_SCROLL_MAX_AMOUNT = 5;
    public int ANDROID_SCROLL_MAX_AMOUNT;

    private String ANDROID_UPPER_STATUS_BAR_HEIGHT_KEY;
    private String ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_ADD_KEY;
    private String ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_SUBTRACT_KEY;
    public int ANDROID_UPPER_STATUS_BAR_HEIGHT;

    private String ANDROID_SCROLL_VERTICAL_WAIT_KEY;
    static final int ANDROID_DEFAULT_SCROLL_VERTICAL_WAIT = 1000;
    public int ANDROID_SCROLL_VERTICAL_WAIT;

    public AndroidTestDeviceConfiguration(TestDevice device) {
        setDeviceConfigurationKeys(device.getName());
        loadDeviceProperties(device);
    }

    private void setDeviceConfigurationKeys(String deviceName) {
        ANDROID_SCROLL_MAX_AMOUNT_KEY = ANDROID_PREFIX + deviceName + ".scrollMaxAmount";
        ANDROID_UPPER_STATUS_BAR_HEIGHT_KEY = ANDROID_PREFIX + deviceName + ".upperStatusBarHeight";
        ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_ADD_KEY = ANDROID_PREFIX + deviceName + ".upperStatusBarHeightAdd";
        ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_SUBTRACT_KEY = ANDROID_PREFIX + deviceName + ".upperStatusBarHeightSubtract";
        ANDROID_SCROLL_VERTICAL_WAIT_KEY = ANDROID_PREFIX + deviceName + ".scrollVerticalWaitAfterPress";
    }

    private void loadDeviceProperties(TestDevice device) {
        ANDROID_UPPER_STATUS_BAR_HEIGHT = getUpperStatusBarHeight(device);
        LOG.debug("Final computed status bar height is " + ANDROID_UPPER_STATUS_BAR_HEIGHT);
        ANDROID_SCROLL_MAX_AMOUNT = properties.getInt(ANDROID_SCROLL_MAX_AMOUNT_KEY, ANDROID_DEFAULT_SCROLL_MAX_AMOUNT);
        ANDROID_SCROLL_VERTICAL_WAIT = properties.getInt(ANDROID_SCROLL_VERTICAL_WAIT_KEY, ANDROID_DEFAULT_SCROLL_VERTICAL_WAIT);
    }

    private int getUpperStatusBarHeight(TestDevice device) {
        int defaultHeight = getDefaultUpperStatusBarHeight(device);
        LOG.debug("Default computed upper status bar height for device '" + device.getName() + "': " + defaultHeight);
        int userDefinedHeight = getUserDefinedUpperStatusBarHeight();
        int positiveCorrection = getPositiveUpperStatusHeightCorrection();
        int negativeCorrection = getNegativeUpperStatusHeightCorrection();

        if (userDefinedHeight > 0) {
            return userDefinedHeight;
        }

        if (positiveCorrection > 0) {
            return defaultHeight + positiveCorrection;
        }

        if (negativeCorrection > 0) {
            return defaultHeight - negativeCorrection;
        }

        return defaultHeight;
    }

    private int getUserDefinedUpperStatusBarHeight() {
        int height = 0;

        try {
            height = properties.getInt(ANDROID_UPPER_STATUS_BAR_HEIGHT_KEY);
        } catch (NoSuchElementException e) {
            LOG.debug("No user set height in configuration file, will be using computed default");
        }

        return height;
    }

    private int getPositiveUpperStatusHeightCorrection() {
        int height = 0;

        try {
            height = properties.getInt(ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_ADD_KEY);
        } catch (NoSuchElementException e) {
            // positive correction not required by config file, do nothing
        }

        return height;
    }

    private int getNegativeUpperStatusHeightCorrection() {
        int height = 0;

        try {
            height = properties.getInt(ANDROID_UPPER_STATUS_BAR_HEIGHT_CORRECTION_SUBTRACT_KEY);
        } catch (NoSuchElementException e) {
            // negative correction not required by config file, do nothing
        }

        return height;
    }

    /*
        Default status bar height is ~4% from device usable viewport height returned by driver.
        More exact value could be ~3.75% for some devices;
     */
    private int getDefaultUpperStatusBarHeight(TestDevice device) {
        return device.getScreenHeight() * 4 / 100;
    }
}
