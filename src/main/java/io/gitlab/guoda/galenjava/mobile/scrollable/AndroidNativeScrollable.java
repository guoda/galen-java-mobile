package io.gitlab.guoda.galenjava.mobile.scrollable;

import io.appium.java_client.android.AndroidDriver;

import io.appium.java_client.MobileElement;
import io.gitlab.guoda.galenjava.mobile.testng.AndroidTestDeviceConfiguration;
import io.gitlab.guoda.galenjava.mobile.utils.DeviceUnderTest;
import org.openqa.selenium.NoSuchElementException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AndroidNativeScrollable implements NativeScrollable {
    private static final Logger LOG = LoggerFactory.getLogger(AndroidNativeScrollable.class);
    private AndroidDriver driver;
    private MobileElement pageTopElement;
    private MobileElement pageBottomElement;
    private boolean pageTopElementIsSetFromPageObject = false;
    private boolean pageBottomElementIsSetFromPageObject = false;

    public AndroidNativeScrollable(AndroidDriver driver) {
        this.driver = driver;
    }

    /**
     * Scrolls to MobileElement on Android device under test.
     * Uses user defined amount of max scrolls to stop scrolling when element is not found from configuration file.
     * @param mobileElement
     */
    @Override
    public void scrollToElement(MobileElement mobileElement) {
        AndroidTestDeviceConfiguration CONFIG = (AndroidTestDeviceConfiguration) DeviceUnderTest.get().getConfiguration();

        LOG.debug("Initializing scroll for device " + DeviceUnderTest.get().getName() + " with max scrolls " + CONFIG.ANDROID_SCROLL_MAX_AMOUNT);
        scrollToElement(mobileElement, CONFIG.ANDROID_SCROLL_MAX_AMOUNT);
    }

    /**
     * Scrolls to MobileElement on Android device under test using max amount of scrolls.
     * Same amount of scrolls is used for scrolling both directions. E.g., when max amount of scrolls is 5:
     * - scroll down 5 times or until bottom element is reached (if defined). If element not found:
     * - scroll up another 5 times or until top element is reached (if defined).
     * When page top or/and page bottom element is not defined, scrolling is always performed {@code maxScrolls} times for both directions.
     * @param mobileElement
     * @param maxScrolls
     */
    @Override
    public void scrollToElement(MobileElement mobileElement, int maxScrolls) {
        try {
            scrollSearchingForElementTopDown(mobileElement, maxScrolls);
        } catch (NoSuchElementException e) {
            throw new NoSuchElementException("No such element found after max scrolls allowed");
        } catch (NoSuchElementScrollLimitReachedException e) {
            throw new NoSuchElementScrollLimitReachedException("No such element found while scrolling from pageTopElement to pageBottomElement");
        }
    }

    /**
     * Sets page bottom element for Android page.
     * @param pageBottomElement
     */
    public void setPageBottomElement(MobileElement pageBottomElement) {
        this.pageBottomElement = pageBottomElement;
        pageBottomElementIsSetFromPageObject = true;
    }

    /**
     * Sets page top element for Android page.
     * @param pageTopElement
     */
    public void setPageTopElement(MobileElement pageTopElement) {
        this.pageTopElement = pageTopElement;
        pageTopElementIsSetFromPageObject = true;
    }

    private void scrollSearchingForElementTopDown(MobileElement mobileElement, int maxScrolls) {
        try {
            scrollDownToElement(mobileElement, maxScrolls);
        } catch (NoSuchElementScrollLimitReachedException e) {
            scrollUpToElement(mobileElement, maxScrolls);
        }
    }

    private void scrollDownToElement(MobileElement mobileElement, int maxScrolls) {
        LOG.debug("Scrolling down to element; " + maxScrolls + " scrolls remaining, device " + DeviceUnderTest.get().getName());
        if (elementIsPresentInViewport(pageBottomElement)) {
            throw new NoSuchElementScrollLimitReachedException("Page bottom of native app reached, no element found");
        }

        scrollDown();

        if (elementIsPresentInViewport(mobileElement)) {
            LOG.debug("Element found while scrolling down; device " + DeviceUnderTest.get().getName());
            return;
        }

        if (maxScrolls == 1) {
            throw new NoSuchElementException("Amount of max scrolls down exceeded, element not found");
        }

        scrollDownToElement(mobileElement, maxScrolls - 1);
    }

    private void scrollUpToElement(MobileElement mobileElement, int maxScrolls) {
        LOG.debug("Scrolling up to element; " + maxScrolls + " scrolls remaining, device " + DeviceUnderTest.get().getName());
        if (elementIsPresentInViewport(pageTopElement)) {
            throw new NoSuchElementScrollLimitReachedException("Page top of native app reached, no element found");
        }

        scrollUp();

        if (elementIsPresentInViewport(mobileElement)) {
            LOG.debug("Element found while scrolling up; device " + DeviceUnderTest.get().getName());
            return;
        }

        if (maxScrolls == 1) {
            throw new NoSuchElementException("Amount of max scrolls up exceeded, element not found");
        }

        scrollUpToElement(mobileElement, maxScrolls - 1);
    }

    private boolean elementIsPresentInViewport(MobileElement element) {
        boolean elementIsPresent = false;

        if (!elementIsSetFromPageObject(element)) {
            return false;
        }

        try {
            if (element.isDisplayed()) {
                elementIsPresent = true;
            }
        } catch (NoSuchElementException e) {
            elementIsPresent = false;
        }
        return elementIsPresent;
    }

    /*
        By default Appium sets locators 'By.id: pageTopElement' and 'By.id: pageBottomElement' for pageTopElement and pageBottomElement which are not
        initialized by user in PO page definitions.
        For scrolling we need those elements to be set in PageObject constructor. If they are not set in PO constructor, don't use them.
        All other elements are set only in PO page definition, so always return 'true' for them.
     */
    private boolean elementIsSetFromPageObject(MobileElement element) {
        if (element == pageTopElement) {
            return pageTopElementIsSetFromPageObject;
        }

        if (element == pageBottomElement) {
            return pageBottomElementIsSetFromPageObject;
        }

        return true;
    }

    protected void scrollDown() {
        AndroidTestDeviceConfiguration CONFIG = (AndroidTestDeviceConfiguration) DeviceUnderTest.get().getConfiguration();

        LOG.debug("Scrolling down for device " + DeviceUnderTest.get().getName());
        AndroidScrollable.scrollDown(driver, DeviceUnderTest.get().getScreenSize(), CONFIG.ANDROID_SCROLL_VERTICAL_WAIT);
    }

    protected void scrollUp() {
        AndroidTestDeviceConfiguration CONFIG = (AndroidTestDeviceConfiguration) DeviceUnderTest.get().getConfiguration();
        LOG.debug("Scrolling up for device " + DeviceUnderTest.get().getName());
        AndroidScrollable.scrollUp(driver, DeviceUnderTest.get().getScreenSize(), CONFIG.ANDROID_SCROLL_VERTICAL_WAIT);
    }
}
